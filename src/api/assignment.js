import Amplify, { API, graphqlOperation } from 'aws-amplify'

import {createAssignment, updateAssignment, deleteAssignment} from '../graphql/mutations'
import {getAssignment, searchAssignments, getUser, listParts, listLevels} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createAssignment, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateAssignment, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(id) {
  try {
    await API.graphql(graphqlOperation(deleteAssignment, 
      {input: {id: id}}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let assignment = await API.graphql(graphqlOperation(getAssignment, {id}))
    assignment = assignment?.data?.getAssignment
    const owner = await API.graphql(graphqlOperation(getUser, {id: assignment.ownerID}))
    assignment.owner = owner?.data?.getUser
    let parts = await API.graphql(graphqlOperation(listParts, {filter: {assignmentID: {eq: assignment.id}}}))
    parts = parts?.data?.listParts?.items
    parts.sort((a, b) => {
      let comparison = 0;
      if (a.name > b.name) {
        comparison = 1;
      } else if (a.name < b.name) {
        comparison = -1;
      }
      return comparison;
    })
    parts = await  Promise.all(parts.map( async (part) => {
      let levels = await API.graphql(graphqlOperation(listLevels, {filter: {partID: {eq: part.id}}}))
      levels = levels?.data?.listLevels?.items
      levels.sort((a, b) => {
        let comparison = 0;
        if (a.name > b.name) {
          comparison = 1;
        } else if (a.name < b.name) {
          comparison = -1;
        }
        return comparison;
      })
      part.levels = levels
      return part
    }))
    assignment.parts = parts
    return Promise.resolve(assignment)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let assignments = await API.graphql(graphqlOperation(searchAssignments))
    assignments = assignments?.data?.searchAssignments?.items
    assignments =  await Promise.all(assignments.map( async (assignment) => {
      const owner = await API.graphql(graphqlOperation(getUser, {id: assignment.ownerID}))
      assignment.owner = owner?.data?.getUser
      let parts = await API.graphql(graphqlOperation(listParts, {filter: {assignmentID: {eq: assignment.id}}}))
      parts = parts?.data?.listParts?.items
      parts.sort((a, b) => {
        let comparison = 0;
        if (a.name > b.name) {
          comparison = 1;
        } else if (a.name < b.name) {
          comparison = -1;
        }
        return comparison;
      })
      assignment.parts = parts
      return assignment
    }))
    return Promise.resolve(assignments)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getFilter(filter) {
  try {
    let assignments = await API.graphql(graphqlOperation(searchAssignments, {filter: filter}))
    assignments = assignments?.data?.searchAssignments?.items
    assignments =  await Promise.all(assignments.map( async (assignment) => {
      const owner = await API.graphql(graphqlOperation(getUser, {id: assignment.ownerID}))
      assignment.owner = owner?.data?.getUser
      let parts = await API.graphql(graphqlOperation(listParts, {filter: {assignmentID: {eq: assignment.id}}}))
      parts = parts?.data?.listParts?.items
      assignment.parts = parts
      return assignment
    }))
    return Promise.resolve(assignments)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  get,
  getAll,
  getFilter,
}