import Amplify, { Auth } from 'aws-amplify'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function signup({username, email, password}) {
  try {
    const { user } = await Auth.signUp({
      username,
      password,
      attributes: {
        email,
      }
    });
    return Promise.resolve(user)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function signin({username, password}) {
  try {
    const user = await Auth.signIn(username, password)
    return Promise.resolve(user)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function signout() {
  try {
    await Auth.signOut()
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function confirm({username, code}) {
  try {
    await Auth.confirmSignUp(username, code);
  } catch(error) {
    return Promise.reject(error)
  }
}

async function reSendConfirmCode(username) {
  try {
    await Auth.resendSignUp(username);
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  signup,
  signin,
  signout,
  confirm,
  reSendConfirmCode,
}
