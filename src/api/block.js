import Amplify, { API, graphqlOperation, Storage } from 'aws-amplify'

import {createStudentBlock, updateStudentBlock, deleteStudentBlock} from '../graphql/mutations'
import {getStudentBlock, listStudentBlocks, getAssignment} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function attach(studentId, assignmentId) {
  try {
    let assignmentTmp = await API.graphql(graphqlOperation(listStudentBlocks, 
      {filter: {
        studentID: {eq: studentId},
        assignmentID: {eq: assignmentId}
      }}))
    console.log(assignmentTmp)
    assignmentTmp = assignmentTmp?.data?.listStudentBlocks?.items
    if (assignmentTmp.length !== 0)
      return Promise.resolve({message: 'block is exist.'})
    await API.graphql(graphqlOperation(createStudentBlock, 
      {input: {studentID: studentId, assignmentID: assignmentId}}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function detach(studentId, assignmentId) {
  try {
    let assignmentTmp = await API.graphql(graphqlOperation(listStudentBlocks, 
      {filter: {
        studentID: {eq: studentId},
        assignmentID: {eq: assignmentId}
      }}))
    assignmentTmp = assignmentTmp?.data?.listStudentBlocks?.items
    if (assignmentTmp.length === 0)
      return Promise.resolve({message: 'block is not exist'})
    
    await API.graphql(graphqlOperation(deleteStudentBlock, 
      {input: {id: assignmentTmp[0].id}}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getBlockAssignments(studentId) {
  try {
    let res = await API.graphql(graphqlOperation(listStudentBlocks, {filter: {studentID: {eq: studentId}}}))
    res = res?.data?.listStudentBlocks?.items
    res = await Promise.all(res.map( async (item) => {
      let assignment = await API.graphql(graphqlOperation(getAssignment, {id: item.assignmentID}))
      assignment = assignment?.data?.getAssignment
      return assignment
    }))
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getBlocks(studentId) {
  try {
    let res = await API.graphql(graphqlOperation(listStudentBlocks, {filter: {studentID: {eq: studentId}}}))
    res = res?.data?.listStudentBlocks?.items
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  attach,
  detach,
  getBlockAssignments,
  getBlocks,
}
