import Amplify, { API, graphqlOperation } from 'aws-amplify'

import {createComment, updateComment, deleteComment, createNewComment, deleteNewComment} from '../graphql/mutations'
import {getComment, listComments, searchComments, searchNewComments} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    let res = await API.graphql(graphqlOperation(createComment, 
      {input: data}))
    res = res?.data?.createComment
    if (data.isOwner) {
      let newComments = await API.graphql(graphqlOperation(searchNewComments, {
        filter: {levelID: {eq: data.levelID}, userID: {eq: data.userID}}
      }))
      newComments = newComments?.data?.searchNewComments?.items
      await Promise.all(newComments.map( async (item) => {
        await API.graphql(graphqlOperation(deleteNewComment, {input: {id: item.id}}))
      }))
    }
    else {
      await API.graphql(graphqlOperation(createNewComment, {
        input: {
          commentID: res?.id,
          levelID: data.levelID,
          userID: data.userID,
        }
      }))
    }
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateComment, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(id) {
  try {
    await API.graphql(graphqlOperation(deleteComment, 
      {input: {id: id}}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let res = await API.graphql(graphqlOperation(getComment, {id}))
    res = res?.data?.getComment
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll(studentId, levelId) {
  try {
    let comments = await API.graphql(graphqlOperation(listComments, {filter: {userID: {eq: studentId}, levelID: {eq: levelId}}}))
    comments = comments?.data?.listComments?.items
    comments.sort((a, b) => {
      let comparison = 0;
      if (a.createdAt > b.createdAt) {
        comparison = 1;
      } else if (a.createdAt < b.createdAt) {
        comparison = -1;
      }
      return comparison;
    })
    console.log(comments)
    return Promise.resolve(comments)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByPagination(studentId, levelId, row, from) {
  try {
    let comments = await API.graphql(graphqlOperation(searchComments, 
      {
        sort: {field: 'createdAt', direction: 'desc'}, 
        from: from, 
        limit: row,
        filter: {userID: {eq: studentId}, levelID: {eq: levelId}},
      }))
    comments = comments?.data?.searchComments?.items
    return Promise.resolve(comments)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  get,
  getAll,
  getByPagination,
}