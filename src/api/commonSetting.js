import Amplify, { API, graphqlOperation } from 'aws-amplify'

import {createCommonSetting, updateCommonSetting} from '../graphql/mutations'
import {getCommonSetting, listCommonSettings} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createCommonSetting, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateCommonSetting, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let common = await API.graphql(graphqlOperation(getCommonSetting, {id}))
    common = common?.data?.getCommonSetting
    return Promise.resolve(common)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let levels = await API.graphql(graphqlOperation(listCommonSettings))
    levels = levels?.data?.listCommonSettings?.items
    return Promise.resolve(levels)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  get,
  getAll,
}
