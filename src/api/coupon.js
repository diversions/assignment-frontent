import Amplify, { API, graphqlOperation, Storage } from 'aws-amplify'

import {createCoupon, updateCoupon, deleteCoupon} from '../graphql/mutations'
import {getCoupon, listCoupons} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createCoupon, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateCoupon, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(id) {
  try {
    await API.graphql(graphqlOperation(deleteCoupon, 
      {input: {id: id}}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function attach(priceId, codes, isFree, discount) {
  try {
    await Promise.all(codes.map(async (code) => {
      await API.graphql(graphqlOperation(createCoupon, {input: {
        priceID: priceId,
        code: code,
        isFree: isFree,
        discountPercentage: discount,
      }}))
    }))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let res = await API.graphql(graphqlOperation(getCoupon, {id}))
    res = res?.data?.getCoupon
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let res = await API.graphql(graphqlOperation(listCoupons))
    res = res?.data?.listCoupons?.items
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getFilter(filter) {
  try {
    let res = await API.graphql(graphqlOperation(listCoupons, {filter}))
    res = res?.data?.listCoupons?.items
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  attach,
  get,
  getAll,
  getFilter,
}
