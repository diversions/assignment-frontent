import Amplify, { API, graphqlOperation } from 'aws-amplify'

import {createExam, updateExam, deleteExam} from '../graphql/mutations'
import {getExam, listExams, ExamsByName, searchExams} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createExam, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateExam, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(id) {
  try {
    await API.graphql(graphqlOperation(deleteExam, 
      {input: {id: id}}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let res = await API.graphql(graphqlOperation(getExam, {id}))
    res = res?.data?.getExam
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getFilter(filter) {
  try {
    let res = await API.graphql(graphqlOperation(listExams, {filter: filter}))
    res = res?.data?.listExams?.items
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let res = await API.graphql(graphqlOperation(listExams))
    res = res?.data?.listExams?.items
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getFirstRow(levelId, row) {
  try {
    let res = await API.graphql(graphqlOperation(listExams, {filter: {levelID: {eq: levelId}}, limit: row}))
    res = res?.data?.listExams?.items
    res.sort((a, b) => {
      let comparison = 0;
      if (a.name > b.name) {
        comparison = 1;
      } else if (a.name < b.name) {
        comparison = -1;
      }
      return comparison;
    })
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByPagination(levelId, row, from) {
  try {
    let res = await API.graphql(graphqlOperation(searchExams, 
      {
        sort: {field: 'name', direction: 'asc'}, 
        from: from, 
        limit: row,
        filter: {levelID: {eq: levelId}}
      }))
    res = res?.data?.searchExams?.items
    
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByLevelId(id) {
  try {
    let res = await API.graphql(graphqlOperation(listExams, {filter: {levelID: {eq: id}}}))
    res = res?.data?.listExams?.items
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  get,
  getFilter,
  getAll,
  getFirstRow,
  getByPagination,
  getByLevelId,
}
