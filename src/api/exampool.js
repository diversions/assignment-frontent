import Amplify, { API, graphqlOperation } from 'aws-amplify'

import {createExamPool, updateExamPool, deleteExamPool} from '../graphql/mutations'
import {getExamPool, listExamPools, searchExamPools, getUser, getAssignment, getLevel, listLevels, getPart, listParts, getExam} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createExamPool, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateExamPool, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(id) {
  try {
    await API.graphql(graphqlOperation(deleteExamPool, 
      {input: {id: id}}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let res = await API.graphql(graphqlOperation(getExamPool, {id}))
    res = res?.data?.getExamPool
    const student = await API.graphql(graphqlOperation(getUser, {id: res.studentID}))
    res.student = student?.data?.getUser
    const assignment = await API.graphql(graphqlOperation(getAssignment, {id: res.assignmentID}))
    res.assignment = assignment?.data?.getAssignment
    const level = await API.graphql(graphqlOperation(getLevel, {id: res.levelID}))
    res.level = level?.data?.getLevel
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getBy(levelId, studentId) {
  try {
    let res = await API.graphql(graphqlOperation(listExamPools, 
      {
        filter: {levelID: {eq: levelId}, studentID: {eq: studentId}},
      }))
    res = res?.data?.listExamPools?.items
    res =  await Promise.all(res.map( async (item) => {
      const exam = await API.graphql(graphqlOperation(getExam, {id: item.examID}))
      item.exam = exam?.data?.getExam
      return item
    }))
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByPagination(row, from) {
  try {
    let res = await API.graphql(graphqlOperation(searchExamPools, 
      {
        sort: {field: 'createdAt', direction: 'desc'},
        from: from,
        limit: row,
      }))
    res = res?.data?.searchExamPools?.items
    res =  await Promise.all(res.map( async (item) => {
      const student = await API.graphql(graphqlOperation(getUser, {id: item.studentID}))
      item.student = student?.data?.getUser
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: item.assignmentID}))
      item.assignment = assignment?.data?.getAssignment
      const level = await API.graphql(graphqlOperation(getLevel, {id: item.levelID}))
      item.level = level?.data?.getLevel
      const exam = await API.graphql(graphqlOperation(getExam, {id: item.examID}))
      item.exam = exam?.data?.getExam
      const part = await API.graphql(graphqlOperation(getPart, {id: item.level.partID}))
      item.part = part?.data?.getPart
      let parts = await API.graphql(graphqlOperation(listParts))
      parts = parts?.data?.listParts?.items
      parts =  parts.filter( (item) => item.assignmentID === item.assignmentID)
      parts = await  Promise.all(parts.map( async (part) => {
        let levels = await API.graphql(graphqlOperation(listLevels))
        levels = levels?.data?.listLevels?.items
        levels = levels.filter((level) => {
          return level.partID === part.id
        })
        part.levels = levels
        return part
      }))
      item.parts = parts
      return item
    }))
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let res = await API.graphql(graphqlOperation(listExamPools))
    res = res?.data?.listExamPools?.items
    res =  await Promise.all(res.map( async (item) => {
      const student = await API.graphql(graphqlOperation(getUser, {id: item.studentID}))
      item.student = student?.data?.getUser
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: item.assignmentID}))
      item.assignment = assignment?.data?.getAssignment
      const level = await API.graphql(graphqlOperation(getLevel, {id: item.levelID}))
      item.level = level?.data?.getLevel
      const exam = await API.graphql(graphqlOperation(getExam, {id: item.examID}))
      item.exam = exam?.data?.getExam
      const part = await API.graphql(graphqlOperation(getPart, {id: item.level.partID}))
      item.part = part?.data?.getPart
      let parts = await API.graphql(graphqlOperation(listParts))
      parts = parts?.data?.listParts?.items
      parts =  parts.filter( (item) => item.assignmentID === item.assignmentID)
      parts = await  Promise.all(parts.map( async (part) => {
        let levels = await API.graphql(graphqlOperation(listLevels))
        levels = levels?.data?.listLevels?.items
        levels = levels.filter((level) => {
          return level.partID === part.id
        })
        part.levels = levels
        return part
      }))
      item.parts = parts
      return item
    }))
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  get,
  getAll,
  getBy,
  getByPagination,
}