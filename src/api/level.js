import Amplify, { API, graphqlOperation } from 'aws-amplify'

import {createLevel, updateLevel, deleteLevel, updateAssignmentStudent} from '../graphql/mutations'
import {getLevel, listLevels, searchLevels, getPart, getAssignment, listAssignmentStudents, listPrograms} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createLevel, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateLevel, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(id) {
  try {
    await API.graphql(graphqlOperation(deleteLevel, 
      {input: {id: id}}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let level = await API.graphql(graphqlOperation(getLevel, {id}))
    level = level?.data?.getLevel
    const part = await API.graphql(graphqlOperation(getPart, {id: level.partID}))
    level.part = part?.data?.getPart
    const assignment = await API.graphql(graphqlOperation(getAssignment, {id: level.part.assignmentID}))
    level.assignment = assignment?.data?.getAssignment
    return Promise.resolve(level)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getFilter(filter) {
  try {
    let levels = await API.graphql(graphqlOperation(listLevels, {filter: filter}))
    levels = levels?.data?.listLevels?.items
    return Promise.resolve(levels)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let levels = await API.graphql(graphqlOperation(listLevels))
    levels = levels?.data?.listLevels?.items
    levels =  await Promise.all(levels.map( async (item) => {
      const part = await API.graphql(graphqlOperation(getPart, {id: item.partID}))
      item.part = part?.data?.getPart
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: item.part.assignmentID}))
      item.assignment = assignment?.data?.getAssignment
      let programs = await API.graphql(graphqlOperation(listPrograms, {filter: {levelID: {eq: item.id}}}))
      programs = programs?.data?.listPrograms?.items
      item.programs = programs
      return item
    }))
    return Promise.resolve(levels)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getFirstRow(partId, row) {
  try {
    let levels = await API.graphql(graphqlOperation(listLevels, {filter: {partID: {eq: partId}}, limit: row}))
    levels = levels?.data?.listLevels?.items
    levels.sort((a, b) => {
      let comparison = 0;
      if (a.name > b.name) {
        comparison = 1;
      } else if (a.name < b.name) {
        comparison = -1;
      }
      return comparison;
    })
    levels =  await Promise.all(levels.map( async (item) => {
      const part = await API.graphql(graphqlOperation(getPart, {id: item.partID}))
      item.part = part?.data?.getPart
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: item.part.assignmentID}))
      item.assignment = assignment?.data?.getAssignment
      let programs = await API.graphql(graphqlOperation(listPrograms, {filter: {levelID: {eq: item.id}}}))
      programs = programs?.data?.listPrograms?.items
      item.programs = programs
      return item
    }))
    return Promise.resolve(levels)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByPagination(partId, row, from) {
  try {
    let levels = await API.graphql(graphqlOperation(searchLevels, 
      {
        sort: {field: 'name', direction: 'asc'}, 
        from: from, 
        limit: row,
        filter: {partID: {eq: partId}}
      }))
    levels = levels?.data?.searchLevels?.items
    levels =  await Promise.all(levels.map( async (item) => {
      const part = await API.graphql(graphqlOperation(getPart, {id: item.partID}))
      item.part = part?.data?.getPart
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: item.part.assignmentID}))
      item.assignment = assignment?.data?.getAssignment
      let programs = await API.graphql(graphqlOperation(listPrograms, {filter: {levelID: {eq: item.id}}}))
      programs = programs?.data?.listPrograms?.items
      item.programs = programs
      return item
    }))
    return Promise.resolve(levels)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByPartId(id) {
  try {
    let levels = await API.graphql(graphqlOperation(listLevels, {filter: {partID: {eq: id}}}))
    levels = levels?.data?.listLevels?.items
    levels =  await Promise.all(levels.map( async (item) => {
      const part = await API.graphql(graphqlOperation(getPart, {id: item.partID}))
      item.part = part?.data?.getPart
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: item.part.assignmentID}))
      item.assignment = assignment?.data?.getAssignment
      let programs = await API.graphql(graphqlOperation(listPrograms, {filter: {levelID: {eq: item.id}}}))
      programs = programs?.data?.listPrograms?.items
      item.programs = programs
      return item
    }))
    return Promise.resolve(levels)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByStudentId(assignmentId, studentId) {
  try {
    let assignmentStudents = await API.graphql(graphqlOperation(listAssignmentStudents))
    assignmentStudents = assignmentStudents?.data?.listAssignmentStudents?.items
    assignmentStudents = assignmentStudents.filter( (item) => item.assignmentID === assignmentId && item.studentID === studentId)
    if (assignmentStudents.length !== 0) {
      let level = await API.graphql(graphqlOperation(getLevel, {id: assignmentStudents[0].levelID}))
      level = level?.data?.getLevel
      let part = await API.graphql(graphqlOperation(getPart, {id: level.partID}))
      level.part = part?.data?.getPart
      let tmp = assignmentStudents[0]
      return Promise.resolve({level: level, assignmentStudent: tmp})
    }
    return Promise.resolve(null)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function setState(data) {
  try {
    await API.graphql(graphqlOperation(updateAssignmentStudent, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  getFilter,
  get,
  getAll,
  getFirstRow,
  getByPagination,
  getByPartId,
  getByStudentId,
  setState,
}
