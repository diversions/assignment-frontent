import Amplify, { API, graphqlOperation } from 'aws-amplify'

import {createNewComment, updateNewComment, deleteNewComment} from '../graphql/mutations'
import {getAssignment, getLevel, getNewComment, getPart, getUser, listNewComments, searchNewComments} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    let res = await API.graphql(graphqlOperation(createNewComment, 
      {input: data}))
    res = res?.data?.createNewComment

    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateNewComment, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(id) {
  try {
    await API.graphql(graphqlOperation(deleteNewComment, 
      {input: {id: id}}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let res = await API.graphql(graphqlOperation(getNewComment, {id}))
    res = res?.data?.getNewComment
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let Newcomments = await API.graphql(graphqlOperation(searchNewComments))
    Newcomments = Newcomments?.data?.searchNewComments?.items
    Newcomments.sort((a, b) => {
      let comparison = 0;
      if (a.createdAt > b.createdAt) {
        comparison = 1;
      } else if (a.createdAt < b.createdAt) {
        comparison = -1;
      }
      return comparison;
    })
    return Promise.resolve(Newcomments)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByPagination(row, from, sort) {
  try {
    let Newcomments = await API.graphql(graphqlOperation(searchNewComments, 
      {
        sort: {field: 'createdAt', direction: sort}, 
        from: from, 
        limit: row,
      }))
    Newcomments = Newcomments?.data?.searchNewComments?.items
    await Promise.all(Newcomments.map( async (item) => {
      const user = await API.graphql(graphqlOperation(getUser, {id: item.userID}))
      item.user = user?.data?.getUser
      let level = await API.graphql(graphqlOperation(getLevel, {id: item.levelID}))
      level = level?.data?.getLevel
      item.level = level
      let part = await API.graphql(graphqlOperation(getPart, {id: level.partID}))
      part = part?.data?.getPart
      item.part = part
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: part.assignmentID}))
      item.assignment = assignment?.data?.getAssignment
      return item
    }))
    return Promise.resolve(Newcomments)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  get,
  getAll,
  getByPagination,
}