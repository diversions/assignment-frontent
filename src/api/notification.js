import Amplify, { API, graphqlOperation } from 'aws-amplify'

import {createNotification, updateNotification, deleteNotification} from '../graphql/mutations'
import {getNotification, listNotifications, searchNotifications, getUser, getAssignment, getLevel, listLevels, getPart, listParts} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createNotification, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateNotification, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(id) {
  try {
    await API.graphql(graphqlOperation(deleteNotification, 
      {input: {id: id}}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let notification = await API.graphql(graphqlOperation(getNotification, {id}))
    notification = notification?.data?.getNotification
    const student = await API.graphql(graphqlOperation(getUser, {id: notification.studentID}))
    notification.student = student?.data?.getUser
    const assignment = await API.graphql(graphqlOperation(getAssignment, {id: notification.assignmentID}))
    notification.assignment = assignment?.data?.getAssignment
    const level = await API.graphql(graphqlOperation(getLevel, {id: notification.levelID}))
    notification.level = level?.data?.getLevel
    return Promise.resolve(notification)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByPagination(row, from) {
  try {
    let notifications = await API.graphql(graphqlOperation(searchNotifications, 
      {
        sort: {field: 'createdAt', direction: 'desc'},
        from: from,
        limit: row,
      }))
    notifications = notifications?.data?.searchNotifications?.items
    notifications =  await Promise.all(notifications.map( async (notification) => {
      const student = await API.graphql(graphqlOperation(getUser, {id: notification.studentID}))
      notification.student = student?.data?.getUser
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: notification.assignmentID}))
      notification.assignment = assignment?.data?.getAssignment
      const level = await API.graphql(graphqlOperation(getLevel, {id: notification.levelID}))
      notification.level = level?.data?.getLevel
      const part = await API.graphql(graphqlOperation(getPart, {id: notification.level.partID}))
      notification.part = part?.data?.getPart
      let parts = await API.graphql(graphqlOperation(listParts))
      parts = parts?.data?.listParts?.items
      parts =  parts.filter( (item) => item.assignmentID === notification.assignmentID)
      parts = await  Promise.all(parts.map( async (part) => {
        let levels = await API.graphql(graphqlOperation(listLevels))
        levels = levels?.data?.listLevels?.items
        levels = levels.filter((level) => {
          return level.partID === part.id
        })
        part.levels = levels
        return part
      }))
      notification.parts = parts
      return notification
    }))
    return Promise.resolve(notifications)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let notifications = await API.graphql(graphqlOperation(listNotifications))
    notifications = notifications?.data?.listNotifications?.items
    notifications =  await Promise.all(notifications.map( async (notification) => {
      const student = await API.graphql(graphqlOperation(getUser, {id: notification.studentID}))
      notification.student = student?.data?.getUser
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: notification.assignmentID}))
      notification.assignment = assignment?.data?.getAssignment
      const level = await API.graphql(graphqlOperation(getLevel, {id: notification.levelID}))
      notification.level = level?.data?.getLevel
      const part = await API.graphql(graphqlOperation(getPart, {id: notification.level.partID}))
      notification.part = part?.data?.getPart
      let parts = await API.graphql(graphqlOperation(listParts))
      parts = parts?.data?.listParts?.items
      parts =  parts.filter( (item) => item.assignmentID === notification.assignmentID)
      parts = await  Promise.all(parts.map( async (part) => {
        let levels = await API.graphql(graphqlOperation(listLevels))
        levels = levels?.data?.listLevels?.items
        levels = levels.filter((level) => {
          return level.partID === part.id
        })
        part.levels = levels
        return part
      }))
      notification.parts = parts
      return notification
    }))
    return Promise.resolve(notifications)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  get,
  getAll,
  getByPagination,
}