import Amplify, { API, graphqlOperation } from 'aws-amplify'

import {createPart, updatePart, deletePart} from '../graphql/mutations'
import {getPart, searchParts, listLevels, getAssignment} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createPart, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updatePart, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(id) {
  try {
    await API.graphql(graphqlOperation(deletePart, 
      {input: {id: id}}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let part = await API.graphql(graphqlOperation(getPart, {id}))
    part = part?.data?.getPart
    const assignment = await API.graphql(graphqlOperation(getAssignment, {id: part.assignmentID}))
    part.assignment = assignment?.data?.getAssignment
    return Promise.resolve(part)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getFilter(filter) {
  try {
    let parts = await API.graphql(graphqlOperation(searchParts, {filter: filter}))
    parts = parts?.data?.searchParts?.items
    return Promise.resolve(parts)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let parts = await API.graphql(graphqlOperation(searchParts))
    parts = parts?.data?.searchParts?.items
    parts =  await Promise.all(parts.map( async (item) => {
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: item.assignmentID}))
      item.assignment = assignment?.data?.getAssignment
      let levels = await API.graphql(graphqlOperation(listLevels, {filter: {partID: {eq: item.id}}}))
      levels = levels?.data?.listLevels?.items
      item.levels = levels
      return item
    }))
    return Promise.resolve(parts)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByAssignmetId(id) {
  try {
    let parts = await API.graphql(graphqlOperation(searchParts, {filter: {assignmentID: {eq: id}}}))
    parts = parts?.data?.searchParts?.items
    parts.sort((a, b) => {
      let comparison = 0;
      if (a.name > b.name) {
        comparison = 1;
      } else if (a.name < b.name) {
        comparison = -1;
      }
      return comparison;
    })
    parts =  await Promise.all(parts.map( async (item) => {
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: item.assignmentID}))
      item.assignment = assignment?.data?.getAssignment
      let levels = await API.graphql(graphqlOperation(listLevels, {filter: {partID: {eq: item.id}}}))
      levels = levels?.data?.listLevels?.items
      levels.sort((a, b) => {
        let comparison = 0;
        if (a.name > b.name) {
          comparison = 1;
        } else if (a.name < b.name) {
          comparison = -1;
        }
        return comparison;
      })
      item.levels = levels
      return item
    }))
    return Promise.resolve(parts)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  get,
  getAll,
  getFilter,
  getByAssignmetId,
}
