import Amplify, { API, graphqlOperation, Storage } from 'aws-amplify'

import {createPartGroup, updatePartGroup, deletePartGroup} from '../graphql/mutations'
import {getPartGroup, listPartGroups} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createPartGroup, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updatePartGroup, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(id) {
  try {
    await API.graphql(graphqlOperation(deletePartGroup, 
      {input: {id: id}}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}


async function replace(sourceId, parts) {
  try {
    let preGroup = await API.graphql(graphqlOperation(listPartGroups, {filter: {sourceID: {eq: sourceId}}}))
    preGroup = preGroup?.data?.listPartGroups?.items
    await Promise.all(preGroup.map( async (item) => {
      await API.graphql(graphqlOperation(deletePartGroup, {input: {id: item.id}}))
    }))
    await Promise.all(parts.map(async (item) => {
      await API.graphql(graphqlOperation(createPartGroup, {input: {
        sourceID: sourceId,
        partID: item.id
      }}))
    }))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function attach(sourceId, parts) {
  try {
    await Promise.all(parts.map(async (item) => {
      await API.graphql(graphqlOperation(createPartGroup, {input: {
        sourceID: sourceId,
        partID: item.id
      }}))
    }))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let res = await API.graphql(graphqlOperation(getPartGroup, {id}))
    res = res?.data?.getPartGroup
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let res = await API.graphql(graphqlOperation(listPartGroups))
    res = res?.data?.listPartGroups?.items
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  attach,
  replace,
  get,
  getAll,
}
