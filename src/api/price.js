import Amplify, { API, graphqlOperation, Storage } from 'aws-amplify'

import {createPrice, updatePrice, deletePrice} from '../graphql/mutations'
import {getPrice, listPrices, searchPrices, getPart, listPartGroups, getAssignment, listStudentPayments} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    let res = await API.graphql(graphqlOperation(createPrice, 
      {input: data}))
    res = res?.data?.createPrice
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updatePrice, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(id) {
  try {
    await API.graphql(graphqlOperation(deletePrice, 
      {input: {id: id}}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let res = await API.graphql(graphqlOperation(getPrice, {id}))
    res = res?.data?.getPrice
    const assignment = await API.graphql(graphqlOperation(getAssignment, {id: res.assignmentID}))
    res.assignment = assignment?.data?.getAssignment
    if (res.type === 'part') {
      const part = await API.graphql(graphqlOperation(getPart, {id: res.sourceID}))
      res.part = part?.data?.getPart
    }
    else if (res.type === 'group') {
      let parts = await API.graphql(graphqlOperation(listPartGroups, {filter: {sourceID: {eq: res.id}}}))
      parts = parts?.data?.listPartGroups?.items
      parts = await Promise.all(parts.map( async (item) => {
        const part = await API.graphql(graphqlOperation(getPart, {id: item.partID}))
        item.part = part?.data?.getPart
        return item
      }))
      parts.sort((a, b) => {
        let comparison = 0;
        if (a.name > b.name) {
          comparison = 1;
        } else if (a.name < b.name) {
          comparison = -1;
        }
        return comparison;
      })
      res.parts = parts
      
    }
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let res = await API.graphql(graphqlOperation(listPrices))
    res = res?.data?.listPrices?.items
    res = await Promise.all(res.map( async (price) => {
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: price.assignmentID}))
      price.assignment = assignment?.data?.getAssignment
      if (price.type === 'part') {
        const part = await API.graphql(graphqlOperation(getPart, {id: price.sourceID}))
        price.part = part?.data?.getPart
      }
      else if (price.type === 'group') {
        let parts = await API.graphql(graphqlOperation(listPartGroups, {filter: {sourceID: {eq: price.id}}}))
        parts = parts?.data?.listPartGroups?.items
        parts = await Promise.all(parts.map( async (item) => {
          const part = await API.graphql(graphqlOperation(getPart, {id: item.partID}))
          item.part = part?.data?.getPart
          return item
        }))
        parts.sort((a, b) => {
          let comparison = 0;
          if (a.name > b.name) {
            comparison = 1;
          } else if (a.name < b.name) {
            comparison = -1;
          }
          return comparison;
        })
        price.parts = parts
      }
      return price
    }))
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getFilter(filter) {
  try {
    let res = await API.graphql(graphqlOperation(listPrices, {filter}))
    res = res?.data?.listPrices?.items
    res = await Promise.all(res.map( async (price) => {
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: price.assignmentID}))
      price.assignment = assignment?.data?.getAssignment
      if (price.type === 'part') {
        const part = await API.graphql(graphqlOperation(getPart, {id: price.sourceID}))
        price.part = part?.data?.getPart
      }
      else if (price.type === 'group') {
        let parts = await API.graphql(graphqlOperation(listPartGroups, {filter: {sourceID: {eq: price.id}}}))
        parts = parts?.data?.listPartGroups?.items
        parts = await Promise.all(parts.map( async (item) => {
          const part = await API.graphql(graphqlOperation(getPart, {id: item.partID}))
          item.part = part?.data?.getPart
          return item
        }))
        parts.sort((a, b) => {
          let comparison = 0;
          if (a.name > b.name) {
            comparison = 1;
          } else if (a.name < b.name) {
            comparison = -1;
          }
          return comparison;
        })
        price.parts = parts
      }
      return price
    }))
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByPagination(row, from) {
  try {
    let res = await API.graphql(graphqlOperation(searchPrices, {
      from: from,
      limit: row,
    }))
    res = res?.data?.searchPrices?.items
    res = await Promise.all(res.map( async (price) => {
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: price.assignmentID}))
      price.assignment = assignment?.data?.getAssignment
      if (price.type === 'part') {
        const part = await API.graphql(graphqlOperation(getPart, {id: price.sourceID}))
        price.part = part?.data?.getPart
      }
      else if (price.type === 'group') {
        let parts = await API.graphql(graphqlOperation(listPartGroups, {filter: {sourceID: {eq: price.id}}}))
        parts = parts?.data?.listPartGroups?.items
        parts = await Promise.all(parts.map( async (item) => {
          const part = await API.graphql(graphqlOperation(getPart, {id: item.partID}))
          item.part = part?.data?.getPart
          return item
        }))
        parts.sort((a, b) => {
          let comparison = 0;
          if (a.name > b.name) {
            comparison = 1;
          } else if (a.name < b.name) {
            comparison = -1;
          }
          return comparison;
        })
        price.parts = parts
      }
      return price
    }))
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

const asyncFilter = async (arr, predicate) => {
	const results = await Promise.all(arr.map(predicate));

	return arr.filter((_v, index) => results[index]);
}

async function getRestPrices(studentId, assignmentId) {
  try {
    console.log('student', studentId)
    console.log('assignment', assignmentId)
    let res = await API.graphql(graphqlOperation(listPrices, {filter: {assignmentID: {eq: assignmentId}}}))
    res = res?.data?.listPrices?.items
    res = await asyncFilter(res, async (price) => {
      let tmp = await API.graphql(graphqlOperation(listStudentPayments, {filter: {priceID: {eq: price.id}, studentID: {eq: studentId}}}))
      tmp = tmp?.data?.listStudentPayments?.items
      return tmp.length === 0
    });
    res = await Promise.all(res.map( async (price) => {
      const assignment = await API.graphql(graphqlOperation(getAssignment, {id: price.assignmentID}))
      price.assignment = assignment?.data?.getAssignment
      if (price.type === 'part') {
        const part = await API.graphql(graphqlOperation(getPart, {id: price.sourceID}))
        price.part = part?.data?.getPart
      }
      else if (price.type === 'group') {
        let parts = await API.graphql(graphqlOperation(listPartGroups, {filter: {sourceID: {eq: price.id}}}))
        parts = parts?.data?.listPartGroups?.items
        parts = await Promise.all(parts.map( async (item) => {
          const part = await API.graphql(graphqlOperation(getPart, {id: item.partID}))
          item.part = part?.data?.getPart
          return item
        }))
        parts.sort((a, b) => {
          let comparison = 0;
          if (a.name > b.name) {
            comparison = 1;
          } else if (a.name < b.name) {
            comparison = -1;
          }
          return comparison;
        })
        price.parts = parts
      }
      return price
    }))
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  get,
  getAll,
  getFilter,
  getByPagination,
  getRestPrices,
}
