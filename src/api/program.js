import Amplify, { API, graphqlOperation } from 'aws-amplify'

import {createProgram, updateProgram, deleteProgram} from '../graphql/mutations'
import {getProgram, listPrograms, programsByName, searchPrograms} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createProgram, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateProgram, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(id) {
  try {
    await API.graphql(graphqlOperation(deleteProgram, 
      {input: {id: id}}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let program = await API.graphql(graphqlOperation(getProgram, {id}))
    program = program?.data?.getProgram
    return Promise.resolve(program)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getFilter(filter) {
  try {
    let programs = await API.graphql(graphqlOperation(listPrograms, {filter: filter}))
    programs = programs?.data?.listPrograms?.items
    return Promise.resolve(programs)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let programs = await API.graphql(graphqlOperation(listPrograms))
    programs = programs?.data?.listPrograms?.items
    return Promise.resolve(programs)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getFirstRow(levelId, row) {
  try {
    let programs = await API.graphql(graphqlOperation(listPrograms, {filter: {levelID: {eq: levelId}}, limit: row}))
    programs = programs?.data?.listPrograms?.items
    programs.sort((a, b) => {
      let comparison = 0;
      if (a.name > b.name) {
        comparison = 1;
      } else if (a.name < b.name) {
        comparison = -1;
      }
      return comparison;
    })
    return Promise.resolve(programs)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByPagination(levelId, row, from) {
  try {
    let programs = await API.graphql(graphqlOperation(searchPrograms, 
      {
        sort: {field: 'name', direction: 'asc'}, 
        from: from, 
        limit: row,
        filter: {levelID: {eq: levelId}}
      }))
    programs = programs?.data?.searchPrograms?.items
    
    return Promise.resolve(programs)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByLevelId(id) {
  try {
    let programs = await API.graphql(graphqlOperation(listPrograms, {filter: {levelID: {eq: id}}}))
    programs = programs?.data?.listPrograms?.items
    return Promise.resolve(programs)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  get,
  getFilter,
  getAll,
  getFirstRow,
  getByPagination,
  getByLevelId,
}
