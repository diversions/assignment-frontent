import Amplify, { API, graphqlOperation, Storage } from 'aws-amplify'

import {createReplit, updateReplit, deleteReplit} from '../graphql/mutations'
import {getReplit, listReplits, searchReplits, getUser} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createReplit, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateReplit, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(id) {
  try {
    await API.graphql(graphqlOperation(deleteReplit, 
      {input: {id: id}}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let res = await API.graphql(graphqlOperation(getReplit, {id}))
    res = res?.data?.getReplit
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let res = await API.graphql(graphqlOperation(listReplits))
    res = res?.data?.listReplits?.items
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getFilter(filter) {
  try {
    let res = await API.graphql(graphqlOperation(listReplits, {filter}))
    res = res?.data?.listReplits?.items
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByPagination(row, from) {
  try {
    let res = await API.graphql(graphqlOperation(searchReplits, {
      from: from,
      limit: row,
    }))
    res = res?.data?.searchReplits?.items
    res = await Promise.all(res.map( async (item) => {
      let user = {}
      if (item.userID && item.userID !== '') {
        user = await API.graphql(graphqlOperation(getUser, {id: item.userID}))
        user = user?.data?.getUser
      }
      item.user = user
      return item
    }))
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  get,
  getAll,
  getFilter,
  getByPagination,
}
