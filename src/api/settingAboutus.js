import Amplify, { API, graphqlOperation } from 'aws-amplify'

import {createAboutus, updateAboutus} from '../graphql/mutations'
import {getAboutus, listAboutuss} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createAboutus, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateAboutus, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let aboutus = await API.graphql(graphqlOperation(getAboutus, {id}))
    aboutus = aboutus?.data?.getAboutus
    return Promise.resolve(aboutus)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let aboutus = await API.graphql(graphqlOperation(listAboutuss))
    aboutus = aboutus?.data?.listAboutuss?.items
    return Promise.resolve(aboutus)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  get,
  getAll,
}
