import Amplify, { API, graphqlOperation, Storage } from 'aws-amplify'

import {createCampus, updateCampus, deleteCampus} from '../graphql/mutations'
import {getCampus, listCampuss} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createCampus, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateCampus, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(data) {
  try {
    await API.graphql(graphqlOperation(deleteCampus, 
      {input: {id: data.id}}))
    await Storage.remove(data.image)
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let common = await API.graphql(graphqlOperation(getCampus, {id}))
    common = common?.data?.getCampus
    return Promise.resolve(common)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let levels = await API.graphql(graphqlOperation(listCampuss))
    levels = levels?.data?.listCampuss?.items
    return Promise.resolve(levels)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  get,
  getAll,
}
