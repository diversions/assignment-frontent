import Amplify, { API, graphqlOperation, Storage } from 'aws-amplify'

import {createSlider, updateSlider, deleteSlider} from '../graphql/mutations'
import {getSlider, listSliders} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createSlider, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateSlider, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(data) {
  try {
    await API.graphql(graphqlOperation(deleteSlider, 
      {input: {id: data.id}}))
    await Storage.remove(data.image)
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let common = await API.graphql(graphqlOperation(getSlider, {id}))
    common = common?.data?.getSlider
    return Promise.resolve(common)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let levels = await API.graphql(graphqlOperation(listSliders))
    levels = levels?.data?.listSliders?.items
    return Promise.resolve(levels)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  get,
  getAll,
}
