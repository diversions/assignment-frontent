import Amplify, { API, graphqlOperation, Storage } from 'aws-amplify'

import {createTestimonial, updateTestimonial, deleteTestimonial} from '../graphql/mutations'
import {getTestimonial, listTestimonials} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    await API.graphql(graphqlOperation(createTestimonial, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateTestimonial, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function remove(data) {
  try {
    await API.graphql(graphqlOperation(deleteTestimonial, 
      {input: {id: data.id}}))
    await Storage.remove(data.avatar)
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    let res = await API.graphql(graphqlOperation(getTestimonial, {id}))
    res = res?.data?.getTestimonial
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    let res = await API.graphql(graphqlOperation(listTestimonials))
    res = res?.data?.listTestimonials?.items
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  remove,
  get,
  getAll,
}
