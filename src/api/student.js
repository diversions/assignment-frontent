import Amplify, { API, graphqlOperation } from 'aws-amplify'

import {createAssignmentStudent, updateAssignmentStudent, deleteAssignmentStudent} from '../graphql/mutations'
import {listAssignmentStudents, searchAssignmentStudents, getAssignment, listAssignments, listParts, getUser} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function addAssignment(data) {
  try {
    let assignments = await API.graphql(graphqlOperation(listAssignmentStudents))
    assignments = assignments?.data?.listAssignmentStudents?.items
    // ongoing assignment
    const ongoingAssignments = assignments.filter( (item) => item.state === 'ongoing' && item.studentID === data.studentID)
    // assignment with current assignment id and student id
    console.log(data)
    assignments = assignments.filter( (item) => item.assignmentID === data.assignmentID && item.studentID === data.studentID)
    if (assignments.length !== 0)
      return Promise.resolve({message: assignments[0].state})
    if (ongoingAssignments.length !== 0)
      return Promise.resolve({message: 'The ongoing assignment exist'})
    await API.graphql(graphqlOperation(createAssignmentStudent, 
      {input: data}))
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function removeWithStudent(studentId, assignmentId) {
  try {
    if (studentId === '' || assignmentId === '')
      return
    let assignment = await API.graphql(graphqlOperation(listAssignmentStudents, {filter: {studentID: {eq: studentId}, assignmentID: {eq: assignmentId}}}))
    assignment = assignment?.data?.listAssignmentStudents?.items
    if (assignment.length !== 0) {
      await API.graphql(graphqlOperation(deleteAssignmentStudent, {input: {id: assignment[0].id}}))
    }
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAssignments(userId) {
  try {
    let allParts = await API.graphql(graphqlOperation(listParts))
    allParts = allParts?.data?.listParts?.items
    // favorite
    let faAssignments = await API.graphql(graphqlOperation(listAssignmentStudents, {filter: {studentID: {eq: userId}}}))
    faAssignments = faAssignments?.data?.listAssignmentStudents?.items
    faAssignments =  await Promise.all(faAssignments.map( async (item) => {
      let assignment = await API.graphql(graphqlOperation(getAssignment, {id: item.assignmentID}))
      assignment = assignment?.data?.getAssignment
      let parts = await API.graphql(graphqlOperation(listParts, {filter: {assignmentID: {eq: item.assignmentID}}}))
      parts = parts?.data?.listParts?.items
      assignment.parts = parts
      assignment.state = item.state
      return assignment
    }))
    // rest
    let reAssignments = await API.graphql(graphqlOperation(listAssignments))
    reAssignments = reAssignments?.data?.listAssignments?.items
    reAssignments = reAssignments.filter((item) => {
      let res = true
      for (let key in faAssignments) {
        if (faAssignments[key].id === item.id) {
          res = false
          break
        }
      }
      if (!item.activate)
        res = false
      return res
    })
    faAssignments = faAssignments.filter((item) => item.activate)
    reAssignments = reAssignments.filter((item) => item.activate)
    reAssignments = await Promise.all(reAssignments.map( async (item) => {
      let parts = await API.graphql(graphqlOperation(listParts, {filter: {assignmentID: {eq: item.id}}}))
      parts = parts?.data?.listParts?.items
      item.parts = parts
      return item
    }))
    const onAssignments = faAssignments.filter((item) => item.state === 'ongoing')
    const completeAssignments = faAssignments.filter((item) => item.state === 'complete')
    const res = {
      ongoing: onAssignments,
      complete: completeAssignments,
      rest: reAssignments,
    }
    return Promise.resolve(res)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAssignmentState(assignmentId, userId) {
  try {
    // favorite
    let assignments = await API.graphql(graphqlOperation(listAssignmentStudents, {filter: {studentID: {eq: userId}, assignmentID: {eq: assignmentId}}}))
    assignments = assignments?.data?.listAssignmentStudents?.items
    if (assignments.length === 0)
      return Promise.resolve('rest')
    else {
      return Promise.resolve(assignments[0].state)
    }
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getStudents(assignmentId) {
  try {
    let assignments = await API.graphql(graphqlOperation(listAssignmentStudents, {filter: {assignmentID: {eq: assignmentId}}}))
    assignments = assignments?.data?.listAssignmentStudents?.items
    const students = await Promise.all(assignments.map( async (item) => {
      let student = await API.graphql(graphqlOperation(getUser, {id:item.studentID}))
      student = student.data.getUser
      return student
    }))
    return Promise.resolve(students)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByPagination(assignmentId, row, from) {
  try {
    let assignments = await API.graphql(graphqlOperation(searchAssignmentStudents, 
      {
        from: from,
        limit: row,
      }))
    assignments = assignments?.data?.searchAssignmentStudents?.items
    const students = await Promise.all(assignments.map( async (item) => {
      let student = await API.graphql(graphqlOperation(getUser, {id:item.studentID}))
      student = student.data.getUser
      return student
    }))
    return Promise.resolve(students)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getStudentsByLevelId(id) {
  try {
    let students = await API.graphql(graphqlOperation(listAssignmentStudents, {filter: {levelID: {eq: id}}}))
    students = students?.data?.listAssignmentStudents?.items
    return Promise.resolve(students)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function setState(assignmentId, studentId, levelState, state) {
  try {
    let assignmentStudents = await API.graphql(graphqlOperation(listAssignmentStudents, {filter: {assignmentID: {eq: assignmentId}, studentID: {eq: studentId}}}))
    assignmentStudents = assignmentStudents?.data?.listAssignmentStudents?.items
    if (assignmentStudents.length !== 0) {
      let tmp = {}
      tmp.id = assignmentStudents[0].id
      tmp.assignmentID = assignmentStudents[0].assignmentID
      tmp.studentID = assignmentStudents[0].studentID
      tmp.levelID = assignmentStudents[0].levelID
      tmp.levelState = levelState
      tmp.state = state
      await API.graphql(graphqlOperation(updateAssignmentStudent, 
        {input: tmp}))
      
      return Promise.resolve({message: 'success'})
    }
    return Promise.resolve(null)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function setLevel(assignmentId, studentId, levelID) {
  try {
    let assignmentStudents = await API.graphql(graphqlOperation(listAssignmentStudents, {filter: {assignmentID: {eq: assignmentId}, studentID: {eq: studentId}}}))
    assignmentStudents = assignmentStudents?.data?.listAssignmentStudents?.items
    if (assignmentStudents.length !== 0) {
      let tmp = {}
      tmp.id = assignmentStudents[0].id
      tmp.levelState = 'ongoing'
      tmp.levelID = levelID
      await API.graphql(graphqlOperation(updateAssignmentStudent, 
        {input: tmp}))
      
      return Promise.resolve({message: 'success'})
    }
    return Promise.resolve(null)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  addAssignment,
  removeWithStudent,
  getAssignments,
  getAssignmentState,
  getStudentsByLevelId,
  getStudents,
  getByPagination,
  setState,
  setLevel,
}