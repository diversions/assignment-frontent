import Amplify, { API, graphqlOperation } from 'aws-amplify'

import {createUser, updateReplit, updateUser} from '../graphql/mutations'
import {getUser, listAssignmentStudents, listUsers, searchUsers, getLevel, getPart, getAssignment, listReplits, listStudentBlocks} from '../graphql/queries'
import awsconfig from '../aws-exports'
Amplify.configure(awsconfig)

async function create(data) {
  try {
    let user = await API.graphql(graphqlOperation(createUser, 
      {input: data}))
    user = user?.data?.createUser
    let replits = await API.graphql(graphqlOperation(listReplits, {filter: {userID: {eq: ''}}}))
    replits = replits?.data?.listReplits?.items
    if (replits.length !== 0) {
      let tmp = {}
      tmp.id = replits[0].id
      tmp.userID = user.id
      await API.graphql(graphqlOperation(updateReplit, {input: tmp}))
    }
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function update(data) {
  try {
    await API.graphql(graphqlOperation(updateUser, 
      {input: data}))
    
    return Promise.resolve({message: 'success'})
  } catch(error) {
    return Promise.reject(error)
  }
}

async function get(id) {
  try {
    const user = await API.graphql(graphqlOperation(getUser, {id}))
    return Promise.resolve(user?.data?.getUser)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByName(name) {
  try {
    let users = await API.graphql(graphqlOperation(searchUsers, {filter: {name: {eq: name}}}))
    users = users?.data?.searchUsers?.items
    return Promise.resolve(users)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getFilter(filter) {
  try {
    let users = await API.graphql(graphqlOperation(listUsers, {filter: filter}))
    users = users?.data?.searchUsers?.items
    return Promise.resolve(users)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getAll() {
  try {
    const users = await API.graphql(graphqlOperation(listUsers))
    return Promise.resolve(users?.data?.listUsers?.items)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getByPagination(row, from) {
  try {
    let users = await API.graphql(graphqlOperation(searchUsers, 
      {
        from: from, 
        limit: row,
      }))
    users = users?.data?.searchUsers?.items
    return Promise.resolve(users)
  } catch(error) {
    return Promise.reject(error)
  }
}

async function getStudent(row, from, filter) {
  try {
    let users = await API.graphql(graphqlOperation(searchUsers, 
      {
        filter: filter,
        from: from, 
        limit: row,
      }))
    users = users?.data?.searchUsers?.items
    users = await Promise.all(users.map( async(item) => {
      let assignments = await API.graphql(graphqlOperation(listAssignmentStudents, {filter: {studentID: {eq: item.id}, state: {eq: 'ongoing'}}}))
      assignments = assignments?.data?.listAssignmentStudents?.items
      let assignment = {}
      if (assignments.length !== 0) {
        const assignmentStuent = assignments[0]
        const assignmentTmp = await API.graphql(graphqlOperation(getAssignment, {id: assignmentStuent.assignmentID}))
        assignment = assignmentTmp?.data?.getAssignment
        const level = await API.graphql(graphqlOperation(getLevel, {id: assignmentStuent.levelID}))
        assignment.level = level?.data?.getLevel
        const part = await API.graphql(graphqlOperation(getPart, {id: assignment.level.partID}))
        assignment.part = part?.data?.getPart
      }
      item.assignment = assignment
      const blocks = await API.graphql(graphqlOperation(listStudentBlocks, {filter: {studentID: {eq: item.id}}}))
      item.blocks = blocks.data.listStudentBlocks.items
      return item
    }))
    return Promise.resolve(users)
  } catch(error) {
    return Promise.reject(error)
  }
}

export {
  create,
  update,
  get,
  getByName,
  getFilter,
  getAll,
  getByPagination,
  getStudent,
}