import React, {useState, useEffect} from 'react'
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
  DialogActions,
  IconButton,
  Backdrop,
  CircularProgress,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import {Edit} from '@material-ui/icons'
import ImageUploader from 'react-images-upload';
import SunEditor from 'suneditor-react'
import 'suneditor/dist/css/suneditor.min.css'
import {NotificationManager} from 'react-notifications'

import FileUpload from '../../../../components/fileUpload'
import MultiFileUpload from '../../../../components/multiFileUpload'
import {useAsync} from '../../../../service/utils'
import {upload} from '../../../../api/file'
import {getRandomString, getFileExtension} from '../../../../service/string'
import {update} from '../../../../api/level'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    textTransform: 'none',
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}))
const EditDialog = (props) => {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const {level, refresh} = props
  const classes = useStyles();
  const [modalActive, setModalActive] = useState(false)
  const [newLevel, setNewLevel] = useState({})
  const [files, setFiles] = useState([])
  const [name, setName] = useState('')
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [image, setImage] = useState(null)
  const [video, setVideo] = useState(null)
  const [otherFiles, setOtherFiles] = useState([])
  const [resDescription, setResDescription] = useState('')
  const [resImage, setResImage] = useState(null)
  const [resVideo, setResVideo] = useState(null)
  const [resOtherFiles, setResOtherFiles] = useState([])
  const [asyncState, setAsyncState] = useState(0)
  const [pending, setPending] = useState(false)

  const handleClickOpen = () => {
    setImage(null)
    setVideo(null)
    setOtherFiles([])
    setResImage(null)
    setResVideo(null)
    setResOtherFiles([])
    setAsyncState(0)
    setModalActive(true)
  }
  const handleClose = () => {
    setModalActive(false)
  }
  const validate = () => {
    let res = true
    if (title === '')
      res = false
    if (description === '')
      res = false
    if (resDescription === '')
      res = false
    if (!res)
      NotificationManager.warning('Please input required fields', 'Worning', 3000);
    return res
  }
  const handleSave = () => {
    if (!validate())
      return
    let files = []
    let tmpLevel = newLevel
    if (image !== null) {
      let tmp = {}
      tmp.filename = `assignment_image_${getRandomString(10)}.${getFileExtension(image.name)}`
      tmp.body = image
      files = [...files, tmp]
      tmpLevel.image = tmp.filename
    }
    if (video !== null) {
      let tmp = {}
      tmp.filename = `assignment_video_${getRandomString(10)}.${getFileExtension(video.name)}`
      tmp.body = video
      files = [...files, tmp]
      tmpLevel.video = tmp.filename
    }
    if (resImage !== null) {
      let tmp = {}
      tmp.filename = `assignment_image_${getRandomString(10)}.${getFileExtension(resImage.name)}`
      tmp.body = resImage
      files = [...files, tmp]
      tmpLevel.resImage = tmp.filename
    }
    if (resVideo !== null) {
      let tmp = {}
      tmp.filename = `assignment_video_${getRandomString(10)}.${getFileExtension(resVideo.name)}`
      tmp.body = resVideo
      files = [...files, tmp]
      tmpLevel.resVideo = tmp.filename
    }
    let tmpOtherFiles = otherFiles.map((item) => {
      return {
        filename: `assignment_others_${getRandomString(10)}.${getFileExtension(item.name)}`,
        body: item,
      }
    })
    let tmpResOtherFiles = resOtherFiles.map((item) => {
      return {
        filename: `assignment_others_${getRandomString(10)}.${getFileExtension(item.name)}`,
        body: item,
      }
    })
    if (tmpOtherFiles.length !== 0) {
      tmpLevel.files = tmpOtherFiles.map((item) => item.filename)
      files = [...files, tmpOtherFiles]
    }
    if (tmpResOtherFiles.length !== 0) {
      tmpLevel.resFiles = tmpResOtherFiles.map((item) => item.filename)
      files = [...files, tmpResOtherFiles]
    }
    tmpLevel.title = title
    tmpLevel.description = description
    tmpLevel.resDescription = resDescription
    setNewLevel(tmpLevel)
    if (files.length === 0) {
      run(update(tmpLevel))
      setAsyncState('update')
    }
    else {
      run(upload(files[0].body, files[0].filename))
      setFiles(files)
      setAsyncState(1)
    }
    setPending(true)
  }
  const fileUpload = () => {
    if (asyncState >= files.length)
      return
    run(upload(files[asyncState].body, files[asyncState].filename))
    setAsyncState(asyncState + 1)
  }
  const onDrop = (file, select) => {
    if (select === 'real')
      setImage(file[0])
    else if (select === 'res')
      setResImage(file[0])
  }
  const changeFile = (file, select) => {
    if (select === 'real')
      setVideo(file)
    else if (select === 'res')
      setResVideo(file)
  }

  useEffect(() => {
    setName(level.name)
    setTitle(level.title)
    setDescription(level.description)
    setResDescription(level.resDescription)
    let tmp = {}
    tmp.id = level.id
    tmp.name = level.name
    tmp.title = level.title
    tmp.description = level.description
    tmp.image = level.image
    tmp.video = level.video
    tmp.files = level.files
    tmp.resDescription = level.resDescription
    tmp.resImage = level.resImage
    tmp.resVideo = level.resVideo
    tmp.resFiles = level.resFiles
    tmp.assignmentID = level.assignmentID
    setNewLevel(tmp)
  }, [level])
  useEffect(() => {
    if (status === 'resolved') {
      if (asyncState < files.length) {
        fileUpload()
        console.log(asyncState)
      }
      else if (asyncState === files.length) {
        run(update(newLevel))
        setAsyncState('update')
      }
      else if (asyncState === 'update') {
        setPending(false)
        refresh()
        setModalActive(false)
      }
    }
    else if (status === 'rejected') {
      console.log(error)
      setPending(false)
    }
  }, [run, status])
  return (
    <>
      <IconButton aria-label="delete" onClick={handleClickOpen}>
        <Edit />
      </IconButton>
      <Backdrop className={classes.backdrop} open={pending} style={{zIndex: 9999}}>
        <CircularProgress color="primary" />
      </Backdrop>
      <Dialog 
        disableBackdropClick
        disableEscapeKeyDown
        open={modalActive} 
        onClose={handleClose} 
        aria-labelledby="form-dialog-title"
        fullWidth
        maxWidth='sm'
      >
        <DialogTitle id="form-dialog-title">Update Level {name}</DialogTitle>
        <DialogContent>
        <DialogContentText>
            Page data
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="title"
            label="Title"
            inputProps={{min: 0, style: { fontSize: 20, paddingTop: 10, paddingBottom: 10 }}}
            type="text"
            fullWidth
            variant="outlined"
            autoComplete="off"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            style={{marginTop: 20, marginBottom: 20}}
          />
          <SunEditor
            showToolbar={true}
            defaultValue={description}
            onChange={setDescription}
            setDefaultStyle="height: auto"
            setOptions={{
              buttonList: [
                [
                  "bold",
                  "underline",
                  "italic",
                  "strike",
                  "list",
                  "align",
                  "fontSize",
                  "formatBlock",
                  "table",
                  "image"
                ]
              ]
            }}
          />
          <FileUpload label={'Choose Video'} changeFile={(file) => changeFile(file, 'real')} />
          <ImageUploader
            withIcon={false}
            withPreview={true}
            singleImage={true}
            buttonText='Choose images'
            onChange={(file) => onDrop(file, 'real')}
            imgExtension={['.jpg', '.gif', '.png', '.gif']}
            maxFileSize={5242880}
          />
          <MultiFileUpload label={'Upload Other Files'} changeFiles={(files) => setOtherFiles(files)} />
          <DialogContentText>
            Restrict Page data
          </DialogContentText>
          <SunEditor
            showToolbar={true}
            defaultValue={resDescription}
            onChange={setResDescription}
            setDefaultStyle="height: auto"
            setOptions={{
              buttonList: [
                [
                  "bold",
                  "underline",
                  "italic",
                  "strike",
                  "list",
                  "align",
                  "fontSize",
                  "formatBlock",
                  "table",
                  "image"
                ]
              ]
            }}
          />
          <FileUpload label={'Choose Video'} changeFile={(file) => changeFile(file, 'res')} />
          <ImageUploader
            withIcon={false}
            withPreview={true}
            singleImage={true}
            buttonText='Choose images'
            onChange={(file) => onDrop(file, 'res')}
            imgExtension={['.jpg', '.gif', '.png', '.gif']}
            maxFileSize={5242880}
          />
          <MultiFileUpload label={'Upload Other Files'} changeFiles={(files) => setResOtherFiles(files)} />
        </DialogContent>
        <DialogActions>
          <Button className={classes.button} onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button className={classes.button} onClick={handleSave} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}
export default EditDialog
