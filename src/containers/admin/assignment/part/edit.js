import React, {useState, useEffect} from 'react'
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
  DialogActions,
  IconButton,
  Backdrop,
  CircularProgress,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import {Edit} from '@material-ui/icons'
import ImageUploader from 'react-images-upload';
import SunEditor from 'suneditor-react'
import 'suneditor/dist/css/suneditor.min.css'
import {NotificationManager} from 'react-notifications'

import FileUpload from '../../../../components/fileUpload'
import {useAsync} from '../../../../service/utils'
import {upload} from '../../../../api/file'
import {getRandomString, getFileExtension} from '../../../../service/string'
import {update} from '../../../../api/part'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    textTransform: 'none',
    fontSize: 15,
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}))
const EditDialog = (props) => {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const {part, refresh} = props
  const classes = useStyles();
  const [modalActive, setModalActive] = useState(false)
  const [newPart, setNewPart] = useState({})
  const [files, setFiles] = useState([])
  const [name, setName] = useState('')
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [image, setImage] = useState(null)
  const [video, setVideo] = useState(null)
  const [asyncState, setAsyncState] = useState(0)
  const [pending, setPending] = useState(false)

  const handleClickOpen = () => {
    setImage(null)
    setVideo(null)
    setAsyncState(0)
    setModalActive(true)
  }
  const handleClose = () => {
    setModalActive(false)
  }
  const validate = () => {
    let res = true
    if (title === '')
      res = false
    if (description === '')
      res = false
    if (!res)
      NotificationManager.warning('Please input required fields', 'Worning', 3000);
    return res
  }
  const handleSave = () => {
    if (!validate())
      return
    let files = []
    let tmpPart = newPart
    if (image !== null) {
      let tmp = {}
      tmp.filename = `assignment_image_${getRandomString(10)}.${getFileExtension(image.name)}`
      tmp.body = image
      files = [...files, tmp]
      tmpPart.image = tmp.filename
    }
    if (video !== null) {
      let tmp = {}
      tmp.filename = `assignment_video_${getRandomString(10)}.${getFileExtension(video.name)}`
      tmp.body = video
      files = [...files, tmp]
      tmpPart.video = tmp.filename
    }
    tmpPart.description = description
    tmpPart.title = title
    setNewPart(tmpPart)
    if (files.length === 0) {
      run(update(tmpPart))
      setAsyncState('update')
    }
    else {
      run(upload(files[0].body, files[0].filename))
      setFiles(files)
      setAsyncState(1)
    }
    setPending(true)
  }
  const fileUpload = () => {
    if (asyncState >= files.length)
      return
    run(upload(files[asyncState].body, files[asyncState].filename))
    setAsyncState(asyncState + 1)
  }
  const onDrop = (file, select) => {
    if (select === 'real')
      setImage(file[0])
  }
  const changeFile = (file, select) => {
    if (select === 'real')
      setVideo(file)
  }

  useEffect(() => {
    setName(part.name)
    setTitle(part.title)
    setDescription(part.description)
    let tmp = {}
    tmp.id = part.id
    tmp.name = part.name
    tmp.title = part.title
    tmp.description = part.description
    tmp.image = part.image
    tmp.video = part.video
    tmp.assignmentID = part.assignmentID
    tmp.levelLength = part.levelLength
    setNewPart(tmp)
  }, [part])
  useEffect(() => {
    if (status === 'resolved') {
      if (asyncState < files.length) {
        fileUpload()
        console.log(asyncState)
      }
      else if (asyncState === files.length) {
        run(update(newPart))
        setAsyncState('update')
      }
      else if (asyncState === 'update') {
        setPending(false)
        refresh()
        setModalActive(false)
      }
    }
    else if (status === 'rejected') {
      console.log(error)
      setPending(false)
    }
  }, [run, status])
  return (
    <>
      <IconButton aria-label="delete" onClick={handleClickOpen}>
        <Edit />
      </IconButton>
      <Backdrop className={classes.backdrop} open={pending} style={{zIndex: 9999}}>
        <CircularProgress color="primary" />
      </Backdrop>
      <Dialog 
        disableBackdropClick
        disableEscapeKeyDown
        open={modalActive} 
        onClose={handleClose} 
        aria-labelledby="form-dialog-title"
        fullWidth
        maxWidth='sm'
      >
        <DialogTitle id="form-dialog-title">Update Part {name}</DialogTitle>
        <DialogContent>
        <DialogContentText>
            Page data
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="title"
            label="Title"
            inputProps={{min: 0, style: { fontSize: 20, paddingTop: 10, paddingBottom: 10 }}}
            type="text"
            fullWidth
            variant="outlined"
            autoComplete="off"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            style={{marginTop: 20, marginBottom: 20}}
          />
          <SunEditor
            showToolbar={true}
            defaultValue={description}
            onChange={setDescription}
            setDefaultStyle="height: auto"
            setOptions={{
              buttonList: [
                [
                  "bold",
                  "underline",
                  "italic",
                  "strike",
                  "list",
                  "align",
                  "fontSize",
                  "formatBlock",
                  "table",
                  "image"
                ]
              ]
            }}
          />
          <FileUpload label={'Choose Video'} changeFile={(file) => changeFile(file, 'real')} />
          <ImageUploader
            withIcon={false}
            withPreview={true}
            singleImage={true}
            buttonText='Choose images'
            onChange={(file) => onDrop(file, 'real')}
            imgExtension={['.jpg', '.gif', '.png', '.gif']}
            maxFileSize={5242880}
          />
        </DialogContent>
        <DialogActions>
          <Button className={classes.button} onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button className={classes.button} onClick={handleSave} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}
export default EditDialog
