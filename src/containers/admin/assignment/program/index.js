import React, {useState, useEffect} from 'react'
import {useParams} from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import {Paper, 
  Table, 
  TableBody, 
  TableCell, 
  TableContainer, 
  TableHead, 
  TableRow, 
  Grid, 
  Container,
  Button,
  IconButton,
  Backdrop,
  CircularProgress,
} from '@material-ui/core'
import {RemoveRedEye, Refresh} from '@material-ui/icons'
import {Link} from 'react-router-dom'

import Nav from '../../../layout/nav_assignment'
import Create from './create'
import Edit from './edit'
import Delete from './delete'
import Back from './back'
import {useAsync} from '../../../../service/utils'
import {getByPagination} from '../../../../api/program'
import {get as getLevel} from '../../../../api/level'
import TablePageNation from '../../../../components/TablePageNation'
import User from './user'
import {sleep} from '../../../../service/common'

const columns = [
  { id: 'name', label: 'name', minWidth: 50 },
  { id: 'code', label: 'Code', minWidth: 100 },
  {
    id: 'action',
    label: 'Action',
    minWidth: 100,
    align: 'center',
  },
]

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
  button: {
    textTransform: 'none',
    color: '#212121',
    borderColor: '#c4c4c4',
    fontSize: 15,
  },
  refresh: {
    float: 'right',
    marginRight: 30,
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}))

const Program = () => {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const {id} = useParams()
  const classes = useStyles()
  const [from, setFrom] = useState(0)
  const [level, setLevel] = useState({})
  const [isEnd, setIsEnd] = useState(false)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [asyncState, setAsyncState] = useState('')
  const [programs, setPrograms] = useState([])
  const [pending, setPending] = useState(false)

  const handlePrev = () => {
    setIsEnd(false)
    let newFrom = from-rowsPerPage
    if (newFrom < 0)
      return
    setFrom(newFrom)
    run(getByPagination(id, rowsPerPage, newFrom))
    setAsyncState('getPrograms')
    setPending(true)
  }
  const handleNext = () => {
    if (isEnd)
      return
    let newFrom = from+rowsPerPage
    setFrom(newFrom)
    run(getByPagination(id, rowsPerPage, newFrom))
    setAsyncState('getPrograms')
    setPending(true)
  }
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value)
    setFrom(0)
  }
  const refresh = (isSleep=true) => {
    if (isSleep)
      sleep(3000)
    run(getByPagination(id, rowsPerPage, 0))
    setAsyncState('getPrograms')
    setPending(true)
    setIsEnd(false)
    setFrom(0)
  }

  useEffect(() => {
    run(getByPagination(id, rowsPerPage, 0))
    setAsyncState('getPrograms')
    setPending(true)
    setIsEnd(false)
    setFrom(0)
  }, [run, id])
  useEffect(() => {
    if (status === 'resolved') {
      // data.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
      if (asyncState === 'getPrograms') {
        if (data.length !== 0) {
          setPrograms(data)
        }
        else {
          setIsEnd(true)
        }
        run(getLevel(id))
        setAsyncState('getLevel')
      }
      else if (asyncState === 'getLevel') {
        setLevel(data)
        setPending(false)
        setAsyncState('')
      }
    }
    else if (status === 'rejected') {
      console.log(error)
      setPending(false)
    }
  }, [status])
  return (
    <>
      <Nav />
      <Backdrop className={classes.backdrop} open={pending}>
        <CircularProgress color="primary" />
      </Backdrop>
      <Container maxWidth="lg">
        <h2 style={{textAlign: 'center', padding: 50}}>Add Program</h2>
        <Create level={level} refresh={refresh} />
        <IconButton className={classes.refresh} aria-label="detail" onClick={() => refresh(false)}>
          <Refresh />
        </IconButton>
        <Back id={id} />
        <Paper className={classes.root}>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth, fontSize: 15 }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {programs.map((row) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align} style={{fontSize: 15}}>
                            {column.id === 'action'?
                              (
                                <>
                                  <Edit program={row} refresh={refresh} />
                                  <Delete program={row} refresh={refresh} level={level} />
                                  <Link to={`/program/${row.id}`} style={{textDecoration: 'none'}}>
                                    <IconButton aria-label="detail">
                                      <RemoveRedEye />
                                    </IconButton>
                                  </Link>
                                </>
                              ):
                              value
                            }
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePageNation 
            enablePrev={from!==0} 
            enableNext={!isEnd} 
            handlePrev={handlePrev} 
            handleNext={handleNext} 
          />
        </Paper>
        <User />
      </Container>
    </>
  )
}

export default Program
