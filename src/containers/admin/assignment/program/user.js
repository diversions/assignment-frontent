import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {Paper, 
  Table, 
  TableBody, 
  TableCell, 
  TableContainer, 
  TableHead, 
  TablePagination, 
  TableRow, 
  Container,
  Button,
  IconButton,
  Backdrop,
  CircularProgress,
} from '@material-ui/core'

import {getByPagination} from '../../../../api/user'
import {useAsync} from '../../../../service/utils'
import TablePageNation from '../../../../components/TablePageNation'

const columns = [
  { id: 'name', label: 'Name', minWidth: 50 },
  { id: 'email', label: 'Email', minWidth: 50 },
]
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 150,
    },
  },
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    padding: 20,
    marginBottom: 40,
    marginTop: 40,
  },
  container: {
    maxHeight: 440,
    paddingBottom: 40
  },
  button: {
    textTransform: 'none',
  },
  title: {
    fontSize: '18px',
    paddingBottom: 30
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}))
const Users = () => {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const classes = useStyles()
  const [from, setFrom] = useState(0)
  const [isEnd, setIsEnd] = useState(false)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [users, setUsers] = useState([])
  const [pending, setPending] = useState(false)

  const handlePrev = () => {
    setIsEnd(false)
    let newFrom = from-rowsPerPage
    if (newFrom < 0)
      return
    setFrom(newFrom)
    run(getByPagination(rowsPerPage, newFrom))
    setPending(true)
  }
  const handleNext = () => {
    if (isEnd)
      return
    let newFrom = from+rowsPerPage
    setFrom(newFrom)
    run(getByPagination(rowsPerPage, newFrom))
    setPending(true)
  }
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value)
    setFrom(0)
  }

  useEffect(() => {
    run(getByPagination(rowsPerPage, 0))
    setPending(true)
    setIsEnd(false)
    setFrom(0)
  }, [run])
  useEffect(() => {
    if (status === 'resolved') {
      console.log(data)
      if (data.length !== 0) {
        setUsers(data)
      }
      else
        setIsEnd(true)
      setPending(false)
    }
    else if (status === 'rejected') {
      console.log(error)
      setPending(false)
    }
  }, [status])
  return (
    <Paper className={classes.root}>
      <div className={classes.title}>Student List</div>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {users.map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePageNation 
        enablePrev={from!==0} 
        enableNext={!isEnd} 
        handlePrev={handlePrev} 
        handleNext={handleNext} 
        style={{paddingTop: 20, float: 'right'}}
      />
    </Paper>
  )
}

export default Users