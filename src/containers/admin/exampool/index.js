import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {Paper, 
  Table, 
  TableBody, 
  TableCell, 
  TableContainer, 
  TableHead, 
  TablePagination, 
  TableRow, 
  Grid, 
  Container,
  Button,
  IconButton,
  Select,
  MenuItem,
  Backdrop,
  CircularProgress,
} from '@material-ui/core'
import {
  RemoveRedEye,
  Refresh
} from '@material-ui/icons'
import {Link} from 'react-router-dom'

import Nav from '../../layout/nav_assignment'
import {getAll, getByPagination, update as updateExamPool} from '../../../api/exampool'
import {update} from '../../../api/user'
import {getFilter as getExams} from '../../../api/exam'
import {setState, setLevel} from '../../../api/student'
import {useAsync} from '../../../service/utils'
import Delete from './delete'
import TablePageNation from '../../../components/TablePageNation'
import {sleep} from '../../../service/common'

const columns = [
  { id: 'assignment', label: 'Assignment', minWidth: 100 },
  { id: 'part', label: 'Part', minWidth: 50 },
  { id: 'level', label: 'Level', minWidth: 50 },
  { id: 'student', label: 'Student', minWidth: 100 },
  { id: 'time', label: 'Time', minWidth: 50 },
  {
    id: 'examDetail',
    label: 'Exam',
    minWidth: 50,
    align: 'center',
  },
  { id: 'state', label: 'State', minWidth: 50 },
  {
    id: 'action',
    label: 'Action',
    minWidth: 300,
    align: 'center',
  },
  { id: 'delete', label: 'Delete', minWidth: 50 },
]

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
  button: {
    textTransform: 'none',
  },
  refresh: {
    cursor: 'pointer',
    fontSize: 25,
    paddingBottom: 4,
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}))
const ExamPool = () => {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const classes = useStyles()
  const [from, setFrom] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [isEnd, setIsEnd] = useState(false)
  const [examPools, setExamPools] = useState([])
  const [examPool, setExamPool] = useState({})
  const [asyncState, setAsyncState] = useState('')
  const [pending, setPending] = useState(false)

  const handlePrev = () => {
    setIsEnd(false)
    let newFrom = from-rowsPerPage
    if (newFrom < 0)
      return
    setFrom(newFrom)
    run(getByPagination(rowsPerPage, newFrom))
    setPending(true)
  }
  const handleNext = () => {
    if (isEnd)
      return
    let newFrom = from+rowsPerPage
    setFrom(newFrom)
    run(getByPagination(rowsPerPage, newFrom))
    setPending(true)
  }
  const handleChangePage = (event, newPage) => {
    setFrom(newPage)
  }
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value)
    setFrom(0)
  }
  const handleAccept = (item) => {
    const level = item.level
    const part = item.part
    const parts = item.parts
    const studentId = item.studentID
    const assignmentId = item.assignmentID
    if (part === parts.length && level === parts[part - 1].levels.length) {
      setState(assignmentId, studentId, 'completed', 'complete')
    }
    else {
      let new_level = []
      if (level < parts[part - 1].levels.length) {
        new_level = parts[part - 1].levels.filter((item) => item.name === level+1)
      }
      else if (level === parts[part - 1].levels.length && part < parts.length) {
        new_level = parts[part].levels.filter((item) => item.name === 1)
      }
      if (new_level.length === 0) {
        setState(assignmentId, studentId, 'completed', 'complete')
      }
      else
        setLevel(assignmentId, studentId, new_level[0].id)
    }
    let tmp = {}
    tmp.id = item.id
    tmp.state = 'accept'
    run(updateExamPool(tmp))
    setAsyncState('update')
    setPending(true)
  }
  const handleReject = (item) => {
    setExamPool(item)
    run(getExams({levelID: {eq: item.levelID}}))
    setAsyncState('getExams')
    setPending(true)
  }
  const refresh = (from) => {
    sleep(2000)
    run(getByPagination(rowsPerPage, from))
    setAsyncState('getExamPools')
    setPending(true)
    setIsEnd(false)
    setFrom(from)
  }

  useEffect(() => {
    run(getByPagination(rowsPerPage, 0))
    setAsyncState('getExamPools')
    setPending(true)
    setIsEnd(false)
    setFrom(0)
  }, [run])
  useEffect(() => {
    if (status === 'resolved') {
      if (asyncState === 'getExamPools') {
        console.log(data)
        if (data.length !== 0) {
          let examPools = data.map((item) => {
            let res = {}
            res.id = item?.id
            res.assignment = item?.assignment?.name
            res.assignmentID = item?.assignmentID
            res.studentID = item?.studentID
            res.levelID = item?.levelID
            res.level = item?.level?.name
            res.part = item?.part?.name
            res.parts = item?.parts
            res.student = item?.student?.name
            res.time = item?.exam?.time
            res.examID = item?.examID
            res.studentObj = item?.student
            res.state = item?.state
            return res
          })
          setExamPools(examPools)
        }
        else {
          setIsEnd(true)
        }
        setPending(false)
      }
      else if (asyncState === 'getExams') {
        if (data.length !== 0) {
          const exam = data[Math.floor(Math.random() * data.length)]
          console.log('exam:', exam)
          let tmp = {}
          tmp.id = examPool.id
          tmp.examID = exam.id
          tmp.state = 'pending'
          run(updateExamPool(tmp))
          setAsyncState('update')
        }
        else
          setPending(false)
      }
      else if (asyncState === 'update') {
        refresh(from)
      }
    }
    else if (status === 'rejected') {
      console.log(error)
      setPending(false)
    }
  }, [status])
  return (
    <div>
      <Nav />
      <Backdrop className={classes.backdrop} open={pending}>
        <CircularProgress color="primary" />
      </Backdrop>
      <Container maxWidth="lg">
        <h2 style={{textAlign: 'center', padding: 50}}>ExamPool Manage</h2>
        <div className="d-flex align-items-center float-right mr-3">
          <Refresh className={classes.refresh} onClick={(e) => refresh(from)} />
        </div>
        <Paper className={classes.root}>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {examPools.map((row) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.id === 'action'?
                              (
                                <>
                                  {row.state !== 'accept'&&
                                  (
                                    <>
                                      <Button 
                                        className={classes.button} 
                                        style={{marginRight: 10}} 
                                        variant="outlined" 
                                        onClick={(e) => handleAccept(row)}
                                      >
                                        Accept
                                      </Button>
                                      <Button 
                                        className={classes.button} 
                                        style={{marginRight: 10}} 
                                        variant="outlined" 
                                        onClick={(e) => handleReject(row)}
                                      >
                                        Reject
                                      </Button>
                                    </>
                                  )
                                  }
                                  
                                </>
                              ):
                              column.id === 'examDetail'?
                              (
                                <Link to={`/exam/${row?.examID}`} style={{textDecoration: 'none'}}>
                                  <IconButton aria-label="detail">
                                    <RemoveRedEye />
                                  </IconButton>
                                </Link>
                              ):
                              column.id === 'delete'?
                              (
                                <Delete item={row} refresh={refresh} />
                              ):
                              value
                            }
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePageNation 
            enablePrev={from!==0} 
            enableNext={!isEnd} 
            handlePrev={handlePrev} 
            handleNext={handleNext} 
          />
        </Paper>
      </Container>
    </div>
  )
}

export default ExamPool