import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {Paper, 
  Table, 
  TableBody, 
  TableCell, 
  TableContainer, 
  TableHead, 
  TablePagination, 
  TableRow, 
  Grid, 
  Container,
  Button,
  IconButton,
  Select,
  MenuItem,
  Backdrop,
  CircularProgress,
} from '@material-ui/core'
import {
  Add, 
  Label, 
  Remove, 
  ArrowBackIos, 
  ArrowForwardIos, 
  Refresh
} from '@material-ui/icons'

import Nav from '../../layout/nav_assignment'
import {getAll, getByPagination, update as updateNotification} from '../../../api/notification'
import {update} from '../../../api/user'
import {download} from '../../../api/file'
import {setState, setLevel} from '../../../api/student'
import {create as createExamPool} from '../../../api/exampool'
import {getFilter as getExams} from '../../../api/exam'
import {useAsync} from '../../../service/utils'
import Delete from './delete'
import TablePageNation from '../../../components/TablePageNation'
import {sleep} from '../../../service/common'

const columns = [
  { id: 'assignment', label: 'Assignment', minWidth: 100 },
  { id: 'part', label: 'Part', minWidth: 50 },
  { id: 'level', label: 'Level', minWidth: 50 },
  { id: 'student', label: 'Student', minWidth: 100 },
  { id: 'helpline', label: 'Helpline', minWidth: 50 },
  {
    id: 'add_helpline',
    label: 'Add Helpline',
    minWidth: 100,
    align: 'center',
  },
  { id: 'state', label: 'State', minWidth: 50 },
  {
    id: 'action',
    label: 'Action',
    minWidth: 300,
    align: 'center',
  },
  { id: 'delete', label: 'Delete', minWidth: 50 },
]

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
  button: {
    textTransform: 'none',
  },
  refresh: {
    cursor: 'pointer',
    fontSize: 25,
    paddingBottom: 4,
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}))
const Notification = () => {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const classes = useStyles()
  const [from, setFrom] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [isEnd, setIsEnd] = useState(false)
  const [notification, setNotification] = useState({})
  const [notifications, setNotifications] = useState([])
  const [asyncState, setAsyncState] = useState('')
  const [pending, setPending] = useState(false)

  const handlePrev = () => {
    setIsEnd(false)
    let newFrom = from-rowsPerPage
    if (newFrom < 0)
      return
    setFrom(newFrom)
    run(getByPagination(rowsPerPage, newFrom))
    setPending(true)
  }
  const handleNext = () => {
    if (isEnd)
      return
    let newFrom = from+rowsPerPage
    setFrom(newFrom)
    run(getByPagination(rowsPerPage, newFrom))
    setPending(true)
  }
  const handleChangePage = (event, newPage) => {
    setFrom(newPage)
  }
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value)
    setFrom(0)
  }
  const changeHelpline = (student, helpline) => {
    let tmp = {}
    tmp.id = student.id
    tmp.name = student.name
    tmp.email = student.email
    tmp.avatar = student.avatar
    tmp.role = student.role
    tmp.helpline = helpline
    run(update(tmp))
    setAsyncState('update')
  }
  const handleDownload = (files) => {
    files.forEach((file) => {
      download(file)
    })
  }
  const handleAccept = (item) => {
    setNotification(item)
    run(getExams({levelID: {eq: item.levelID}}))
    setAsyncState('getExams')
    setPending(true)
  }
  const handleReject = (item) => {
    const studentId = item.studentID
    const assignmentId = item.assignmentID
    setState(assignmentId, studentId, 'ongoing', 'ongoing')
    let tmp = {}
    tmp.id = item.id
    tmp.assignmentID = item.assignmentID
    tmp.studentID = item.studentID
    tmp.levelID = item.levelID
    tmp.files = item.files
    tmp.state = 'reject'
    run(updateNotification(tmp))
    setAsyncState('update')
    setPending(true)
  }
  const refresh = (from) => {
    sleep(3000)
    run(getByPagination(rowsPerPage, from))
    setAsyncState('getNotifications')
    setPending(true)
    setIsEnd(false)
    setFrom(from)
  }

  useEffect(() => {
    run(getByPagination(rowsPerPage, 0))
    setAsyncState('getNotifications')
    setPending(true)
    setIsEnd(false)
    setFrom(0)
  }, [run])
  useEffect(() => {
    if (status === 'resolved') {
      if (asyncState === 'getNotifications') {
        if (data.length !== 0) {
          let notifications = data.map((item) => {
            let res = {}
            res.id = item?.id
            res.assignment = item?.assignment?.name
            res.assignmentID = item?.assignmentID
            res.studentID = item?.studentID
            res.levelID = item?.levelID
            res.level = item?.level?.name
            res.part = item?.part?.name
            res.parts = item?.parts
            res.student = item?.student?.name
            res.helpline = item?.student?.helpline
            res.studentObj = item?.student
            res.files = item?.files
            res.state = item?.state
            return res
          })
          setNotifications(notifications)
        }
        else {
          setIsEnd(true)
        }
        setPending(false)
      }
      else if (asyncState === 'getExams') {
        if (data.length !== 0) {
          const exam = data[Math.floor(Math.random() * data.length)]
          console.log('exam:', exam)
          let examPool = {}
          examPool.assignmentID = notification.assignmentID
          examPool.studentID = notification.studentID
          examPool.levelID = notification.levelID
          examPool.state = 'pending'
          examPool.examID = exam.id
          console.log('exam pool', examPool)
          createExamPool(examPool)
          setState(notification.assignmentID, notification.studentID, 'exam', 'ongoing')
          let tmp = {}
          tmp.id = notification.id
          tmp.assignmentID = notification.assignmentID
          tmp.studentID = notification.studentID
          tmp.levelID = notification.levelID
          tmp.files = notification.files
          tmp.state = 'accept'
          run(updateNotification(tmp))
          setAsyncState('update')
        }
        else 
          setPending(false)
      }
      else if (asyncState === 'update') {
        refresh(from)
      }
    }
    else if (status === 'rejected') {
      console.log(error)
      setPending(false)
    }
  }, [status])
  return (
    <div>
      <Nav />
      <Backdrop className={classes.backdrop} open={pending}>
        <CircularProgress color="primary" />
      </Backdrop>
      <Container maxWidth="lg">
        <h2 style={{textAlign: 'center', padding: 50}}>Notification Manage</h2>
        <div className="d-flex align-items-center float-right mr-3">
          <Refresh className={classes.refresh} onClick={(e) => refresh(from)} />
        </div>
        <Paper className={classes.root}>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {notifications.map((row) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.id === 'action'?
                              (
                                <>
                                  <Button className={classes.button} style={{marginRight: 10}} variant="outlined" onClick={(e) => handleDownload(row.files)}>Doanload</Button>
                                  {row.state === 'pending'&&
                                  (
                                    <>
                                      <Button 
                                        className={classes.button} 
                                        style={{marginRight: 10}} 
                                        variant="outlined" 
                                        onClick={(e) => handleAccept(row)}
                                      >
                                        Accept
                                      </Button>
                                      <Button 
                                        className={classes.button} 
                                        style={{marginRight: 10}} 
                                        variant="outlined" 
                                        onClick={(e) => handleReject(row)}
                                      >
                                        Reject
                                      </Button>
                                    </>
                                  )
                                  }
                                  
                                </>
                              ):
                              column.id === 'add_helpline'?
                              (
                                <>
                                  <IconButton aria-label="detail" onClick={(e) => changeHelpline(row.studentObj, row.helpline + 1)}>
                                    <Add />
                                  </IconButton>
                                  <IconButton aria-label="detail" onClick={(e) => changeHelpline(row.studentObj, row.helpline - 1)}>
                                    <Remove />
                                  </IconButton>
                                </>
                              ):
                              column.id === 'delete'?
                              (
                                <Delete notification={row} refresh={refresh} />
                              ):
                              value
                            }
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePageNation 
            enablePrev={from!==0} 
            enableNext={!isEnd} 
            handlePrev={handlePrev} 
            handleNext={handleNext} 
          />
        </Paper>
      </Container>
    </div>
  )
}

export default Notification