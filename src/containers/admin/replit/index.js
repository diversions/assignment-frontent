import React, {useEffect, useState} from 'react'
import {
  Paper,
  Grid,
  Table, 
  TableBody, 
  TableCell, 
  TableContainer, 
  TableHead, 
  TablePagination, 
  TableRow,
  Container,
  IconButton,
  Backdrop,
  CircularProgress,
} from '@material-ui/core'
import {Refresh} from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'

import Nav from '../../layout/nav_assignment'
import {useAsync} from '../../../service/utils'
import {getByPagination} from '../../../api/replit'
import Create from './create'
import Edit from './edit'
import Delete from './delete'
import TablePageNation from '../../../components/TablePageNation'
import {sleep} from '../../../service/common'

const columns = [
  { id: 'name', label: 'Replit Username', minWidth: 100 },
  { id: 'password', label: 'Replit Password', minWidth: 100 },
  { id: 'userEmail', label: 'User Email', minWidth: 100 },
  {
    id: 'action',
    label: 'Action',
    minWidth: 170,
    align: 'center',
  },
]
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  title: {
    fontSize: '18px',
    paddingBottom: 30
  },
  button: {
    textTransform: 'none',
    fontSize: 15,
  },
  refresh: {
    float: 'right',
    marginRight: 30,
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}))
const Replit = () => {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const classes = useStyles()
  const [from, setFrom] = useState(0)
  const [isEnd, setIsEnd] = useState(false)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [replits, setReplits] = useState([])
  const [pending, setPending] = useState(false)

  const handlePrev = () => {
    setIsEnd(false)
    let newFrom = from-rowsPerPage
    if (newFrom < 0)
      return
    setFrom(newFrom)
    run(getByPagination(rowsPerPage, newFrom))
    setPending(true)
  }
  const handleNext = () => {
    if (isEnd)
      return
    let newFrom = from+rowsPerPage
    setFrom(newFrom)
    run(getByPagination(rowsPerPage, newFrom))
    setPending(true)
  }
  const handleChangePage = (event, newPage) => {
    setFrom(newPage)
  }
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value)
    setFrom(0)
  }
  const refresh = (isSleep=true) => {
    if (isSleep)
      sleep(2000)
    run(getByPagination(rowsPerPage, from))
    setPending(true)
    setIsEnd(false)
    setFrom(from)
  }

  useEffect(() => {
    run(getByPagination(rowsPerPage, 0))
    setPending(true)
    setIsEnd(false)
    setFrom(0)
  }, [run])
  useEffect(() => {
    if (status === 'resolved') {
      let tmp = data.map((item) => {
        item.userEmail = item.user.email
        return item
      })
      setReplits(tmp)
      setPending(false)
    }
    else if (status === 'rejected') {
      console.log(error)
      setPending(false)
    }
  }, [status])
  return (
    <div>
      <Nav />
      <Backdrop className={classes.backdrop} open={pending}>
        <CircularProgress color="primary" />
      </Backdrop>
      <Container maxWidth="lg">
        <h2 style={{textAlign: 'center', padding: 50}}>Replit User Manage</h2>
        <Create refresh={refresh} />
        <IconButton className={classes.refresh} aria-label="detail" onClick={() => refresh(false)}>
          <Refresh />
        </IconButton>
        <Paper className={classes.root}>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth, fontSize: 15 }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {replits.map((row, index) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align} style={{fontSize: 14}}>
                            {column.id === 'action'?
                              (
                                <>
                                  <Edit item={row} refresh={refresh} />
                                  <Delete item={row} refresh={refresh} />
                                </>
                              ):
                              value
                            }
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePageNation 
            enablePrev={from!==0} 
            enableNext={!isEnd} 
            handlePrev={handlePrev} 
            handleNext={handleNext} 
          />
        </Paper>
      </Container>
    </div>
  )
}

export default Replit