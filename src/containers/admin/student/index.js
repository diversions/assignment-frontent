import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Paper, 
  Table, 
  TableBody, 
  TableCell, 
  TableContainer, 
  TableHead, 
  TablePagination, 
  TableRow, 
  Grid, 
  Container,
  Button,
  IconButton,
  Select,
  MenuItem,
  Backdrop,
  CircularProgress,
} from '@material-ui/core'
import {Add, Label, Remove} from '@material-ui/icons'

import Nav from '../../layout/nav_assignment'
import {getAll} from '../../../api/assignment'
import {getByPagination} from '../../../api/student'
import {update} from '../../../api/user'
import {useAsync} from '../../../service/utils'
import TablePageNation from '../../../components/TablePageNation'
import Manage from './manage'

const columns = [
  { id: 'name', label: 'Name', minWidth: 100 },
  { id: 'helpline', label: 'Helpline', minWidth: 50 },
  {
    id: 'action',
    label: 'Action',
    minWidth: 170,
    align: 'center',
  },
]
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 150,
    },
  },
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
  button: {
    textTransform: 'none',
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}))
const Student = () => {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const classes = useStyles()
  const [from, setFrom] = useState(0)
  const [isEnd, setIsEnd] = useState(false)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [assignments, setAssignments] = useState([])
  const [assignment, setAssignment] = useState('')
  const [students, setStudents] = useState([])
  const [asyncState, setAsyncState] = useState('')
  const [pending, setPending] = useState(false)

  const handlePrev = () => {
    setIsEnd(false)
    let newFrom = from-rowsPerPage
    if (newFrom < 0)
      return
    setFrom(newFrom)
    run(getByPagination(assignment, rowsPerPage, newFrom))
    setPending(true)
    setAsyncState('getStudents')
  }
  const handleNext = () => {
    if (isEnd)
      return
    let newFrom = from+rowsPerPage
    setFrom(newFrom)
    run(getByPagination(assignment, rowsPerPage, newFrom))
    setPending(true)
    setAsyncState('getStudents')
  }
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value)
    setFrom(0)
  }
  const handleChangeAssignment = (event) => {
    const assignmentId = event.target.value
    setAssignment(assignmentId)
    run(getByPagination(assignmentId, rowsPerPage, 0))
    setFrom(0)
    setAsyncState('getStudents')
    setPending(true)
  }
  const changeHelpline = (student, helpline) => {
    let tmp = {}
    tmp.id = student.id
    tmp.name = student.name
    tmp.email = student.email
    tmp.avatar = student.avatar
    tmp.role = student.role
    tmp.helpline = helpline
    run(update(tmp))
    setAsyncState('update')
    setPending(true)
  }

  useEffect(() => {
    run(getAll())
    setAsyncState('getAll')
    setPending(true)
  }, [run])
  useEffect(() => {
    if (status === 'resolved') {
      if (asyncState === 'getAll') {
        setAssignments(data)
        if (data.length !== 0) {
          setAssignment(data[0].id)
          run(getByPagination(data[0].id, rowsPerPage, 0))
          setFrom(0)
          setAsyncState('getStudents')
        }
      }
      else if (asyncState === 'getStudents') {
        if (data.length !== 0) {
          setStudents(data)
        }
        else 
          setIsEnd(true)
        setPending(false)
      }
      else if (asyncState === 'update') {
        run(getByPagination(assignment, rowsPerPage, 0))
        setFrom(0)
        setAsyncState('getStudents')
        setPending(false)
      }
    }
    else if (status === 'rejected') {
      console.log(error)
      setPending(false)
    }
  }, [run, status])

  return (
    <div>
      <Nav />
      <Backdrop className={classes.backdrop} open={pending}>
        <CircularProgress color="primary" />
      </Backdrop>
      <Container maxWidth="lg">
        <h2 style={{textAlign: 'center', padding: 50}}>Student Manage</h2>
        <Grid container spacing={3}>
          <Grid item lg={3} md={4} sm={6} xs={12}>
            <Select
              style={{width: '100%', textAlign: 'center', marginBottom: 10}}
              value={assignment}
              onChange={handleChangeAssignment}
              MenuProps={MenuProps}
            >
              {assignments.map((item) => (
                <MenuItem key={item.id} value={item.id} style={{}}>
                  {item.name}
                </MenuItem>
              ))}
            </Select>
          </Grid>
        </Grid>
        <Paper className={classes.root}>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {students.map((row) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.id === 'action'?
                              (
                                <>
                                  <IconButton aria-label="detail" onClick={(e) => changeHelpline(row, row.helpline + 1)}>
                                    <Add />
                                  </IconButton>
                                  <IconButton aria-label="detail" onClick={(e) => changeHelpline(row, row.helpline - 1)}>
                                    <Remove />
                                  </IconButton>
                                </>
                              ):
                              value
                            }
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePageNation 
            enablePrev={from!==0} 
            enableNext={!isEnd} 
            handlePrev={handlePrev} 
            handleNext={handleNext} 
            style={{paddingTop: 20, float: 'right'}}
          />
        </Paper>
        <Manage />
      </Container>
    </div>
  )
}

export default Student