import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Paper, 
  Table, 
  TableBody, 
  TableCell, 
  TableContainer, 
  TableHead, 
  TablePagination, 
  TableRow, 
  Grid, 
  Container,
  Button,
  IconButton,
  Select,
  TextField,
  MenuItem,
  Backdrop,
  CircularProgress,
} from '@material-ui/core'
import {Add, Label, Remove} from '@material-ui/icons'

import {getStudent} from '../../../api/user'
import {useAsync} from '../../../service/utils'
import TablePageNation from '../../../components/TablePageNation'
import {sleep} from '../../../service/common'
import Change from './change'
import Block from './block'

const columns = [
  { id: 'userName', label: 'User Name', minWidth: 50 },
  { id: 'assignmentName', label: 'Assignment', minWidth: 100 },
  { id: 'partName', label: 'Part', minWidth: 100 },
  { id: 'levelName', label: 'Level', minWidth: 100 },
  { id: 'blockNumber', label: 'Block', minWidth: 5 },
  {
    id: 'action',
    label: 'Action',
    minWidth: 170,
    align: 'center',
  },
]
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 150,
    },
  },
};
const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
  button: {
    textTransform: 'none',
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}))
const Student = () => {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const classes = useStyles()
  const [from, setFrom] = useState(0)
  const [isEnd, setIsEnd] = useState(false)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [filterKey, setFilterKey] = useState('name')
  const [filterString, setFilterString] = useState('')
  const [students, setStudents] = useState([])
  const [asyncState, setAsyncState] = useState('')
  const [pending, setPending] = useState(false)

  const handlePrev = () => {
    setIsEnd(false)
    let newFrom = from-rowsPerPage
    if (newFrom < 0)
      return
    setFrom(newFrom)
    run(getStudent(rowsPerPage, newFrom))
    setPending(true)
  }
  const handleNext = () => {
    if (isEnd)
      return
    let newFrom = from+rowsPerPage
    setFrom(newFrom)
    run(getStudent(rowsPerPage, newFrom))
    setPending(true)
  }
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value)
    setFrom(0)
  }
  const handleChangeKey = (event) => {
    setFilterKey(event.target.value)
  }
  const changeSearch = (event) => {
    const string = event.target.value
    setFilterString(string)
    refresh(0)
  }
  const refresh = (from=from) => {
    sleep(2000)
    let filter = {role: {eq: 'student'}}
    if (filterString !== '')
      filter[filterKey] = {match: filterString}
    console.log(filter)
    run(getStudent(rowsPerPage, from, filter))
    setPending(true)
  }

  useEffect(() => {
    let filter = {role: {eq: 'student'}}
    run(getStudent(rowsPerPage, 0, filter))
    setFrom(0)
    setIsEnd(false)
  }, [run, rowsPerPage])
  useEffect(() => {
    if (status === 'resolved') {
      console.log(data)
      if (data.length !== 0) {
        let tmp = data.map((item) => {
          item.userName = item.name
          item.assignmentName = item?.assignment?.name
          item.partName = item?.assignment?.part?.name
          item.levelName = item?.assignment?.level?.name
          item.blockNumber = item?.blocks?.length
          return item
        })
        setStudents(tmp)
      }
      else {
        setIsEnd(true)
      }
      setPending(false)
    }
    else if (status === 'rejected') {
      console.log(error)
      setPending(false)
    }
  }, [run, status])

  return (
    <div>
      <Backdrop className={classes.backdrop} open={pending}>
        <CircularProgress color="primary" />
      </Backdrop>
      <h2 style={{textAlign: 'center', padding: 50}}>Assignment Manage</h2>
      {/* <Grid container spacing={3} alignItems="center">
        <Grid item lg={3} md={4} sm={6} xs={12}>
          <Select
            style={{width: '100%', textAlign: 'center', marginBottom: 10}}
            value={filterKey}
            onChange={handleChangeKey}
            MenuProps={MenuProps}
          >
            <MenuItem value="name">
              User Name
            </MenuItem>
            <MenuItem value="email">
              User Email
            </MenuItem>
          </Select>
        </Grid>
        <Grid item lg={3} md={4} sm={6} xs={12}>
          <TextField
            autoFocus
            margin="dense"
            id="filterString"
            label="Search"
            inputProps={{min: 0, style: { fontSize: 20, paddingTop: 5, paddingBottom: 5 }}}
            type="text"
            fullWidth
            variant="outlined"
            autoComplete="off"
            value={filterString}
            onChange={changeSearch}
            style={{marginTop: 20}}
          />
        </Grid>
        <Grid item lg={3} md={4} sm={6} xs={12}>
          <Button className={classes.button} style={{}} variant="outlined" onClick={Search}>Search</Button>
        </Grid>
      </Grid> */}
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {students.map((row) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id} align={column.align}>
                          {column.id === 'action'?
                            (
                              <>
                                <Change refresh={refresh} item={row} />
                                <Block refresh={refresh} item={row} />
                              </>
                            ):
                            column.id === 'blockNumber' && value > 0?
                            <span style={{color: 'red'}}>{value}</span>:
                            value
                          }
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePageNation 
          enablePrev={from!==0} 
          enableNext={!isEnd} 
          handlePrev={handlePrev} 
          handleNext={handleNext} 
          style={{paddingTop: 20, float: 'right'}}
        />
      </Paper>
    </div>
  )
}

export default Student