import React, { useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Container, Row, Col } from 'react-bootstrap'
import {NotificationManager} from 'react-notifications'

import Header from '../../components/NavTwoStudent'
import { BreadcrumbBox } from '../../components/common/Breadcrumb'
import FooterTwo from '../../components/FooterTwo'
import { Styles } from './styles/account.js'
import {setCookie} from '../../service/cookie'
import {useSetting} from '../../provider/setting'
import {useAsync} from '../../service/utils'
import {signin} from '../../api/auth'
import {getByName, getAll} from '../../api/user'

function Login() {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const history = useHistory()
  const [, dispatch] = useSetting()
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [asyncState, setAsyncState] = useState('')

  const validate = () => {
    let res = true
    if (username === '')
      res = false
    else if (password === '')
      res = false
    return res
  }
  const handleSignin = () => {
    if (!validate())
      return
    run(signin({username, password}))
    setAsyncState('signin')
  }

  useEffect(() => {
    if (status === 'resolved') {
      if (asyncState === 'signin') {
        run(getByName(username))
        setAsyncState('getUser')
      }
      else if (asyncState === 'getUser') {
        console.log("user", data)
        if (data.length !== 0) {
          const user = data[0]
          dispatch({type: 'SET', settingName: 'auth', settingData: user})
          setCookie('auth', JSON.stringify(user), 1)
          if (user?.role === 'student')
            history.push('/course')
          else if (user?.role === 'owner')
            history.push('/assignment')
        }
        setAsyncState('')
      }
    }
    else if (status === 'rejected') {
      NotificationManager.warning(error?.message, 'Worning', 3000);
      console.log(error)
    }
  }, [status])
  return (
    <Styles>
      {/* Main Wrapper */}
      <div className="main-wrapper login-page">

        {/* Header 2 */}
        <Header />

        {/* Breadcroumb */}
        <BreadcrumbBox title="Log In" />

        {/* Login Area */}
        <section className="login-area">
          <Container>
            <Row>
              <Col md="12">
                <div className="login-box">
                  <div className="login-title text-center">
                    <h3>Log In</h3>
                  </div>
                  <div id="form_login" className="form">
                    <p className="form-control">
                      <label htmlFor="login_user">User Name</label>
                      <input 
                        type="text" 
                        placeholder="Username" 
                        id="login_user" 
                        autoComplete="off"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                      />
                      <span className="login_input-msg"></span>
                    </p>
                    <p className="form-control">
                      <label htmlFor="login_password">Password</label>
                      <input 
                        type="password" 
                        placeholder="*******" 
                        id="login_password" 
                        autoComplete="off"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                      />
                      <span className="login_input-msg"></span>
                    </p>
                    <button onClick={handleSignin}>Log In</button>
                    <div className="save-forget-password d-flex justify-content-between">
                      <div className="save-passowrd">
                        <label htmlFor="save_password"><input type="checkbox" id="save_password" className="check-box" />Save Password</label>
                      </div>
                      <div className="forget-password">
                        <Link to={process.env.PUBLIC_URL + "/"}>Forget Password?</Link>
                      </div>
                    </div>
                    <div className="not_account-btn text-center">
                      <p>Haven't Any Account Yet? <Link to={process.env.PUBLIC_URL + "/register"}>Click Here</Link></p>
                    </div>
                    <div className="social-login text-center">
                      <p>Login With Social</p>
                      <ul className="list-unstyled list-inline">
                        <li className="list-inline-item"><a href={process.env.PUBLIC_URL + "/"}><i className="fab fa-google"></i> Google</a></li>
                        <li className="list-inline-item"><a href={process.env.PUBLIC_URL + "/"}><i className="fab fa-facebook-f"></i> Facebook</a></li>
                        <li className="list-inline-item"><a href={process.env.PUBLIC_URL + "/"}><i className="fab fa-twitter"></i> Twitter</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </section>

        {/* Footer 2 */}
        <FooterTwo />

      </div>
    </Styles>
  )
}

export default Login