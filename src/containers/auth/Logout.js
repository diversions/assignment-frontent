import React, {useEffect} from 'react'
import {Link, useHistory} from 'react-router-dom'

import {useSetting} from '../../provider/setting'
import {useAsync} from '../../service/utils'
import {signout} from '../../api/auth'
import {setCookie} from '../../service/cookie'

const Logout = () => {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const [, dispatch] = useSetting()
  const history = useHistory()

  useEffect(() => {
    run(signout())
  }, [])
  useEffect(() => {
    if (status === 'resolved') {
      dispatch({type: 'SET', settingName: 'auth', settingData: null})
      setCookie('auth', '', 0)
      history.push('/login')
    }
    else if (status === 'rejected') {
      console.log(error)
    }
  }, [status])
  return (
    <div></div>
  )
}
export default Logout
