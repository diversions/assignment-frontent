import React, { useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Container, Row, Col } from 'react-bootstrap'
import {NotificationManager} from 'react-notifications'

import Header from '../../components/NavTwoStudent'
import { BreadcrumbBox } from '../../components/common/Breadcrumb'
import FooterTwo from '../../components/FooterTwo'
import { Styles } from './styles/account.js'
import {useAsync} from '../../service/utils'
import {isEmail} from '../../service/string'
import {signup} from '../../api/auth'
import {create, getFilter as getUser} from '../../api/user'

function Register() {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const history = useHistory()
  const [username, setUsername] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [asyncState, setAsyncState] = useState('')

  const validate = () => {
    let res = true
    if (username === '')
      res = false
    else if (email === '' || !isEmail(email))
      res = false
    else if (password === '')
      res = false
    if (!res)
      NotificationManager.warning('Please input required fields', 'Worning', 3000);
    return res
  }
  const signUp = () => {
    if (!validate())
      return
    // run(getUser({email: {eq: email}}))
    // setAsyncState('getUser')
    run(signup({username, email, password}))
    setAsyncState('signup')
  }

  useEffect(() => {
    if (status === 'resolved') {
      if (asyncState === 'getUser') {
        if (data.length === 0) {
          run(signup({username, email, password}))
          setAsyncState('signup')
        }
        else {
          NotificationManager.warning('User with this email exist now.', 'Worning', 3000);
          setPassword('')
        }
      }
      else if (asyncState === 'signup') {
        run(create({
          name: username,
          email: email,
          avatar: '/default-avatar.jpg',
          role: 'student',
          helpline: 10,
        }))
        setAsyncState('create')
      }
      else if (asyncState === 'create') {
        history.push('/confirm')
      }
    }
    else if (status === 'rejected') {
      NotificationManager.warning(error?.message, 'Worning', 3000);
      console.log(error)
    }
  }, [status])

  return (
    <Styles>
      {/* Main Wrapper */}
      <div className="main-wrapper registration-page">

        {/* Header 2 */} 
        <Header />

        {/* Breadcroumb */}
        <BreadcrumbBox title="Registration" />

        {/* Registration Area */}
        <section className="registration-area">
          <Container>
            <Row>
              <Col md="12">
                <div className="registration-box">
                  <div className="registration-title text-center">
                    <h3>Registration</h3>
                  </div>
                  <div id="form_registration" className="form">
                    <p className="form-control">
                      <label htmlFor="registration_user">User Name</label>
                      <input 
                        type="text" 
                        placeholder="Username" 
                        id="registration_user" 
                        autoComplete="off"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                      />
                      <span className="registration_input-msg"></span>
                    </p>
                    <p className="form-control">
                      <label htmlFor="registration_email">Email Address</label>
                      <input 
                        type="email" 
                        placeholder="Email address" 
                        id="registration_email" 
                        autoComplete="off"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                      />
                      <span className="registration_input-msg"></span>
                    </p>
                    <p className="form-control">
                      <label htmlFor="registration_password">Password</label>
                      <input 
                        type="password" 
                        placeholder="*******" 
                        id="registration_password" 
                        autoComplete="off"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                      />
                      <span className="registration_input-msg"></span>
                    </p>
                    <button onClick={signUp}>Register Now</button>
                  </div>
                  <div className="have_account-btn text-center">
                    <p>Already have an account? <Link to="/login">Login Here</Link></p>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </section>

        {/* Footer 2 */}
        <FooterTwo />

      </div>
    </Styles>
  )
}

export default Register