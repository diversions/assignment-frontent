import React, {useEffect, useState} from 'react'
import {Link, useHistory} from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import {
  AppBar,
  Toolbar,
  Button,
  IconButton,
  Typography,
  Menu,
  MenuItem,
} from '@material-ui/core'
import {Menu as MenuIcon} from '@material-ui/icons'

import {useSetting} from '../../provider/setting'
import {useAsync} from '../../service/utils'
import {signout} from '../../api/auth'
import {setCookie} from '../../service/cookie'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  button: {
    textTransform: 'none',
    marginRight: 15,
    color: 'white',
    fontSize: 15,
  }
}));

export default function Nav() {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const history = useHistory()
  const classes = useStyles()
  const [setting, dispatch] = useSetting()
  const [comment, setComment] = useState(null);

  const handleLogout = () => {
    run(signout())
  }
  const handleClick = (event) => {
    setComment(event.currentTarget);
  }
  const handleClose = () => {
    setComment(null);
  }

  useEffect(() => {
    if (status === 'resolved') {
      dispatch({type: 'SET', settingName: 'auth', settingData: null})
      setCookie('auth', '', 0)
      history.push('/login')
    }
    else if (status === 'rejected') {
      console.log(error)
    }
  }, [status])
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            <Link to="/assignment" style={{textDecoration: 'none'}}>
              <Button className={classes.button} color="inherit">
                Assignment
              </Button>
            </Link>
            <Link to="/student" style={{textDecoration: 'none'}}>
              <Button className={classes.button} color="inherit">
                Student
              </Button>
            </Link>
            <Button className={classes.button} color="inherit" onClick={handleClick}>
              Comment
            </Button>
            <Menu
              anchorEl={comment}
              keepMounted
              open={Boolean(comment)}
              onClose={handleClose}
            >
              <Link to="/comment" style={{textDecoration: 'none'}}>
                <MenuItem onClick={handleClose}>New Comment</MenuItem>
              </Link>
              <Link to="/comment/all" style={{textDecoration: 'none'}}>
                <MenuItem onClick={handleClose}>All Comments</MenuItem>
              </Link>
            </Menu>
            <Link to="/notification" style={{textDecoration: 'none'}}>
              <Button className={classes.button} color="inherit">
                Notification
              </Button>
            </Link>
            <Link to="/exampool" style={{textDecoration: 'none'}}>
              <Button className={classes.button} color="inherit">
                ExamPool
              </Button>
            </Link>
            <Link to="/newsletter" style={{textDecoration: 'none'}}>
              <Button className={classes.button} color="inherit">
                NewsLetter
              </Button>
            </Link>
            <Link to="/replit" style={{textDecoration: 'none'}}>
              <Button className={classes.button} color="inherit">
                Replit
              </Button>
            </Link>
            <Link to="/payment" style={{textDecoration: 'none'}}>
              <Button className={classes.button} color="inherit">
                Payment
              </Button>
            </Link>
            <Link to="/home-setting" style={{textDecoration: 'none'}}>
              <Button className={classes.button} color="inherit">
                Home Setting
              </Button>
            </Link>
          </Typography>
          <Button className={classes.button} color="inherit" onClick={handleLogout}>Logout</Button>
        </Toolbar>
      </AppBar>
    </div>
  )
}