import React, {useState, useEffect} from 'react'
import {
  Backdrop,
  CircularProgress,
} from '@material-ui/core'
import {AttachFile} from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'

import {upload} from '../../../../api/file'
import {useAsync} from '../../../../service/utils'
import {getRandomString, getFileExtension} from '../../../../service/string'

const useStyles = makeStyles((theme) => ({
  root: {
    fontFamily: 'sans-serif',
    textAlign: 'center',
  },
  uploadLabel: {
    display: 'flex',
    alignItems: 'center',
    padding: '6px 12px',
    cursor: 'pointer',
    fontSize: 15,
  },
  uploadInput: {
    display: 'none'
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}))
const CommentFileAttach = (props) => {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const {callback, ...rest} = props
  const classes = useStyles()
  const [files, setFiles] = useState([])
  const [filenames, setFilenames] = useState([])
  const [fileState, setFileState] = useState(-1)
  const [pending, setPending] = useState(false)

  const onChange = (e) => {
    let tmp = [...e.target.files]
    console.log(tmp)
    if (tmp.length !== 0) {
      tmp = tmp.map((item) => {
        const filename = `assignment_files_${getRandomString(10)}.${getFileExtension(item.name)}`
        return {
          name: filename,
          body: item,
        }
      })
      setFiles(tmp)
      const tmpFilenames = tmp.map((item) => item.name)
      setFilenames(tmpFilenames)
      run(upload(tmp[0].body, tmp[0].name))
      setFileState(1)
      setPending(true)
    }
  }
  const fileUpload = () => {
    if (fileState >= files.length)
      return
    run(upload(files[fileState].body, files[fileState].name))
    setFileState(fileState + 1)
  }

  useEffect(() => {
    if (status === 'resolved') {
      if (fileState < files.length) {
        fileUpload()
        console.log(fileState)
      }
      else if (fileState === files.length) {
        callback(filenames)
        setFileState(-1)
        setPending(false)
      }
    }
    else if (status === 'rejected') {
      console.log(error)
      setPending(false)
    }
  }, [run, status])
  return (
    <div {...rest}>
      <Backdrop className={classes.backdrop} open={pending} style={{zIndex: 9999}}>
        <CircularProgress color="primary" />
      </Backdrop>
      <label className={classes.uploadLabel}>
        <input className={classes.uploadInput} multiple type="file" onChange={onChange} />
        <AttachFile />
      </label>
    </div>
  )
}
export default CommentFileAttach
