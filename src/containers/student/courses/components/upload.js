import React, {useState, useEffect} from 'react'
import { makeStyles } from '@material-ui/core/styles'

import MultiFileUpload from '../../../../components/multiFileUpload'
import {useAsync} from '../../../../service/utils'
import {useSetting} from '../../../../provider/setting'
import {upload} from '../../../../api/file'
import {getRandomString, getFileExtension} from '../../../../service/string'
import {create} from '../../../../api/notification'
import {setState} from '../../../../api/level'
import { Styles } from '../styles/upload.js'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    textTransform: 'none',
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}))
const UploadForm = (props) => {
  const {data, status, error, run} = useAsync({
    status: 'idle',
  })
  const [setting] = useSetting()
  const {assignmentId, levelId, assignmentStudent, refresh} = props
  const classes = useStyles();
  const [files, setFiles] = useState([])
  const [filenames, setFilenames] = useState([])
  const [asyncState, setAsyncState] = useState(0)
  const [pending, setPending] = useState(false)

  const validate = () => {
    let res = true
    if (files.length === 0)
      res = false
    return res
  }
  const handleUpload = () => {
    if (!validate())
      return
    fileUpload()
    setPending(true)
  }
  const fileUpload = () => {
    if (asyncState === files.length)
      return
    let filename = `assignment_image_${getRandomString(10)}.${getFileExtension(files[asyncState].name)}`
    run(upload(files[asyncState], filename))
    setAsyncState(asyncState + 1)
    setFilenames([...filenames, filename])
  }
  const changeFiles = (files) => {
    setFiles(files)
  }

  useEffect(() => {
    if (status === 'resolved') {
      if (asyncState < files.length) {
        fileUpload()
        console.log(asyncState)
      }
      else if (asyncState === files.length) {
        const notification = {
          assignmentID: assignmentId,
          studentID: setting.auth.id,
          levelID: levelId,
          files: filenames,
          state: 'pending',
        }
        run(create(notification))
        setAsyncState('create')
      }
      else if (asyncState === 'create') {
        let tmp = {}
        tmp.id = assignmentStudent.id
        tmp.assignmentID = assignmentStudent.assignmentID
        tmp.studentID = assignmentStudent.studentID
        tmp.levelID = assignmentStudent.levelID
        tmp.levelState = 'evaluating'
        tmp.state = assignmentStudent.state
        run(setState(tmp))
        setAsyncState('setState')
      }
      else if (asyncState === 'setState') {
        setPending(false)
        refresh()
      }
    }
    else if (status === 'rejected') {
      console.log(error)
      setPending(false)
    }
  }, [run, status])
  return (
    <Styles>
        <MultiFileUpload label={'Upload Files'} changeFiles={changeFiles} />
        <div className="d-flex justify-content-center">
            <button onClick={handleUpload} color="primary">
                Upload
            </button>
        </div>
    </Styles>
  )
}
export default UploadForm
