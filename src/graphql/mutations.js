/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createUser = /* GraphQL */ `
  mutation CreateUser(
    $input: CreateUserInput!
    $condition: ModelUserConditionInput
  ) {
    createUser(input: $input, condition: $condition) {
      id
      name
      email
      avatar
      role
      helpline
      createdAt
      updatedAt
    }
  }
`;
export const updateUser = /* GraphQL */ `
  mutation UpdateUser(
    $input: UpdateUserInput!
    $condition: ModelUserConditionInput
  ) {
    updateUser(input: $input, condition: $condition) {
      id
      name
      email
      avatar
      role
      helpline
      createdAt
      updatedAt
    }
  }
`;
export const deleteUser = /* GraphQL */ `
  mutation DeleteUser(
    $input: DeleteUserInput!
    $condition: ModelUserConditionInput
  ) {
    deleteUser(input: $input, condition: $condition) {
      id
      name
      email
      avatar
      role
      helpline
      createdAt
      updatedAt
    }
  }
`;
export const createReplit = /* GraphQL */ `
  mutation CreateReplit(
    $input: CreateReplitInput!
    $condition: ModelReplitConditionInput
  ) {
    createReplit(input: $input, condition: $condition) {
      id
      name
      password
      userID
      createdAt
      updatedAt
    }
  }
`;
export const updateReplit = /* GraphQL */ `
  mutation UpdateReplit(
    $input: UpdateReplitInput!
    $condition: ModelReplitConditionInput
  ) {
    updateReplit(input: $input, condition: $condition) {
      id
      name
      password
      userID
      createdAt
      updatedAt
    }
  }
`;
export const deleteReplit = /* GraphQL */ `
  mutation DeleteReplit(
    $input: DeleteReplitInput!
    $condition: ModelReplitConditionInput
  ) {
    deleteReplit(input: $input, condition: $condition) {
      id
      name
      password
      userID
      createdAt
      updatedAt
    }
  }
`;
export const createAssignment = /* GraphQL */ `
  mutation CreateAssignment(
    $input: CreateAssignmentInput!
    $condition: ModelAssignmentConditionInput
  ) {
    createAssignment(input: $input, condition: $condition) {
      id
      name
      title
      description
      fee
      image
      ownerID
      activate
      review
      studentNumber
      startDate
      Duration
      language
      skillLevel
      subject
      lectures
      enrolled
      createdAt
      updatedAt
    }
  }
`;
export const updateAssignment = /* GraphQL */ `
  mutation UpdateAssignment(
    $input: UpdateAssignmentInput!
    $condition: ModelAssignmentConditionInput
  ) {
    updateAssignment(input: $input, condition: $condition) {
      id
      name
      title
      description
      fee
      image
      ownerID
      activate
      review
      studentNumber
      startDate
      Duration
      language
      skillLevel
      subject
      lectures
      enrolled
      createdAt
      updatedAt
    }
  }
`;
export const deleteAssignment = /* GraphQL */ `
  mutation DeleteAssignment(
    $input: DeleteAssignmentInput!
    $condition: ModelAssignmentConditionInput
  ) {
    deleteAssignment(input: $input, condition: $condition) {
      id
      name
      title
      description
      fee
      image
      ownerID
      activate
      review
      studentNumber
      startDate
      Duration
      language
      skillLevel
      subject
      lectures
      enrolled
      createdAt
      updatedAt
    }
  }
`;
export const createSubject = /* GraphQL */ `
  mutation CreateSubject(
    $input: CreateSubjectInput!
    $condition: ModelSubjectConditionInput
  ) {
    createSubject(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const updateSubject = /* GraphQL */ `
  mutation UpdateSubject(
    $input: UpdateSubjectInput!
    $condition: ModelSubjectConditionInput
  ) {
    updateSubject(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const deleteSubject = /* GraphQL */ `
  mutation DeleteSubject(
    $input: DeleteSubjectInput!
    $condition: ModelSubjectConditionInput
  ) {
    deleteSubject(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const createAssignmentStudent = /* GraphQL */ `
  mutation CreateAssignmentStudent(
    $input: CreateAssignmentStudentInput!
    $condition: ModelAssignmentStudentConditionInput
  ) {
    createAssignmentStudent(input: $input, condition: $condition) {
      id
      assignmentID
      studentID
      levelID
      levelState
      state
      createdAt
      updatedAt
    }
  }
`;
export const updateAssignmentStudent = /* GraphQL */ `
  mutation UpdateAssignmentStudent(
    $input: UpdateAssignmentStudentInput!
    $condition: ModelAssignmentStudentConditionInput
  ) {
    updateAssignmentStudent(input: $input, condition: $condition) {
      id
      assignmentID
      studentID
      levelID
      levelState
      state
      createdAt
      updatedAt
    }
  }
`;
export const deleteAssignmentStudent = /* GraphQL */ `
  mutation DeleteAssignmentStudent(
    $input: DeleteAssignmentStudentInput!
    $condition: ModelAssignmentStudentConditionInput
  ) {
    deleteAssignmentStudent(input: $input, condition: $condition) {
      id
      assignmentID
      studentID
      levelID
      levelState
      state
      createdAt
      updatedAt
    }
  }
`;
export const createStudentBlock = /* GraphQL */ `
  mutation CreateStudentBlock(
    $input: CreateStudentBlockInput!
    $condition: ModelStudentBlockConditionInput
  ) {
    createStudentBlock(input: $input, condition: $condition) {
      id
      studentID
      assignmentID
      createdAt
      updatedAt
    }
  }
`;
export const updateStudentBlock = /* GraphQL */ `
  mutation UpdateStudentBlock(
    $input: UpdateStudentBlockInput!
    $condition: ModelStudentBlockConditionInput
  ) {
    updateStudentBlock(input: $input, condition: $condition) {
      id
      studentID
      assignmentID
      createdAt
      updatedAt
    }
  }
`;
export const deleteStudentBlock = /* GraphQL */ `
  mutation DeleteStudentBlock(
    $input: DeleteStudentBlockInput!
    $condition: ModelStudentBlockConditionInput
  ) {
    deleteStudentBlock(input: $input, condition: $condition) {
      id
      studentID
      assignmentID
      createdAt
      updatedAt
    }
  }
`;
export const createPart = /* GraphQL */ `
  mutation CreatePart(
    $input: CreatePartInput!
    $condition: ModelPartConditionInput
  ) {
    createPart(input: $input, condition: $condition) {
      id
      name
      title
      description
      image
      video
      assignmentID
      levelLength
      createdAt
      updatedAt
    }
  }
`;
export const updatePart = /* GraphQL */ `
  mutation UpdatePart(
    $input: UpdatePartInput!
    $condition: ModelPartConditionInput
  ) {
    updatePart(input: $input, condition: $condition) {
      id
      name
      title
      description
      image
      video
      assignmentID
      levelLength
      createdAt
      updatedAt
    }
  }
`;
export const deletePart = /* GraphQL */ `
  mutation DeletePart(
    $input: DeletePartInput!
    $condition: ModelPartConditionInput
  ) {
    deletePart(input: $input, condition: $condition) {
      id
      name
      title
      description
      image
      video
      assignmentID
      levelLength
      createdAt
      updatedAt
    }
  }
`;
export const createPrice = /* GraphQL */ `
  mutation CreatePrice(
    $input: CreatePriceInput!
    $condition: ModelPriceConditionInput
  ) {
    createPrice(input: $input, condition: $condition) {
      id
      price
      type
      assignmentID
      sourceID
      createdAt
      updatedAt
    }
  }
`;
export const updatePrice = /* GraphQL */ `
  mutation UpdatePrice(
    $input: UpdatePriceInput!
    $condition: ModelPriceConditionInput
  ) {
    updatePrice(input: $input, condition: $condition) {
      id
      price
      type
      assignmentID
      sourceID
      createdAt
      updatedAt
    }
  }
`;
export const deletePrice = /* GraphQL */ `
  mutation DeletePrice(
    $input: DeletePriceInput!
    $condition: ModelPriceConditionInput
  ) {
    deletePrice(input: $input, condition: $condition) {
      id
      price
      type
      assignmentID
      sourceID
      createdAt
      updatedAt
    }
  }
`;
export const createCoupon = /* GraphQL */ `
  mutation CreateCoupon(
    $input: CreateCouponInput!
    $condition: ModelcouponConditionInput
  ) {
    createCoupon(input: $input, condition: $condition) {
      id
      priceID
      code
      isFree
      discountPercentage
      createdAt
      updatedAt
    }
  }
`;
export const updateCoupon = /* GraphQL */ `
  mutation UpdateCoupon(
    $input: UpdateCouponInput!
    $condition: ModelcouponConditionInput
  ) {
    updateCoupon(input: $input, condition: $condition) {
      id
      priceID
      code
      isFree
      discountPercentage
      createdAt
      updatedAt
    }
  }
`;
export const deleteCoupon = /* GraphQL */ `
  mutation DeleteCoupon(
    $input: DeleteCouponInput!
    $condition: ModelcouponConditionInput
  ) {
    deleteCoupon(input: $input, condition: $condition) {
      id
      priceID
      code
      isFree
      discountPercentage
      createdAt
      updatedAt
    }
  }
`;
export const createPartGroup = /* GraphQL */ `
  mutation CreatePartGroup(
    $input: CreatePartGroupInput!
    $condition: ModelpartGroupConditionInput
  ) {
    createPartGroup(input: $input, condition: $condition) {
      id
      partID
      sourceID
      createdAt
      updatedAt
    }
  }
`;
export const updatePartGroup = /* GraphQL */ `
  mutation UpdatePartGroup(
    $input: UpdatePartGroupInput!
    $condition: ModelpartGroupConditionInput
  ) {
    updatePartGroup(input: $input, condition: $condition) {
      id
      partID
      sourceID
      createdAt
      updatedAt
    }
  }
`;
export const deletePartGroup = /* GraphQL */ `
  mutation DeletePartGroup(
    $input: DeletePartGroupInput!
    $condition: ModelpartGroupConditionInput
  ) {
    deletePartGroup(input: $input, condition: $condition) {
      id
      partID
      sourceID
      createdAt
      updatedAt
    }
  }
`;
export const createStudentPayment = /* GraphQL */ `
  mutation CreateStudentPayment(
    $input: CreateStudentPaymentInput!
    $condition: ModelstudentPaymentConditionInput
  ) {
    createStudentPayment(input: $input, condition: $condition) {
      id
      priceID
      studentID
      createdAt
      updatedAt
    }
  }
`;
export const updateStudentPayment = /* GraphQL */ `
  mutation UpdateStudentPayment(
    $input: UpdateStudentPaymentInput!
    $condition: ModelstudentPaymentConditionInput
  ) {
    updateStudentPayment(input: $input, condition: $condition) {
      id
      priceID
      studentID
      createdAt
      updatedAt
    }
  }
`;
export const deleteStudentPayment = /* GraphQL */ `
  mutation DeleteStudentPayment(
    $input: DeleteStudentPaymentInput!
    $condition: ModelstudentPaymentConditionInput
  ) {
    deleteStudentPayment(input: $input, condition: $condition) {
      id
      priceID
      studentID
      createdAt
      updatedAt
    }
  }
`;
export const createLevel = /* GraphQL */ `
  mutation CreateLevel(
    $input: CreateLevelInput!
    $condition: ModelLevelConditionInput
  ) {
    createLevel(input: $input, condition: $condition) {
      id
      name
      title
      description
      image
      video
      files
      resDescription
      resImage
      resVideo
      partID
      resFiles
      programLength
      examLength
      createdAt
      updatedAt
    }
  }
`;
export const updateLevel = /* GraphQL */ `
  mutation UpdateLevel(
    $input: UpdateLevelInput!
    $condition: ModelLevelConditionInput
  ) {
    updateLevel(input: $input, condition: $condition) {
      id
      name
      title
      description
      image
      video
      files
      resDescription
      resImage
      resVideo
      partID
      resFiles
      programLength
      examLength
      createdAt
      updatedAt
    }
  }
`;
export const deleteLevel = /* GraphQL */ `
  mutation DeleteLevel(
    $input: DeleteLevelInput!
    $condition: ModelLevelConditionInput
  ) {
    deleteLevel(input: $input, condition: $condition) {
      id
      name
      title
      description
      image
      video
      files
      resDescription
      resImage
      resVideo
      partID
      resFiles
      programLength
      examLength
      createdAt
      updatedAt
    }
  }
`;
export const createNotification = /* GraphQL */ `
  mutation CreateNotification(
    $input: CreateNotificationInput!
    $condition: ModelNotificationConditionInput
  ) {
    createNotification(input: $input, condition: $condition) {
      id
      assignmentID
      studentID
      levelID
      files
      state
      createdAt
      updatedAt
    }
  }
`;
export const updateNotification = /* GraphQL */ `
  mutation UpdateNotification(
    $input: UpdateNotificationInput!
    $condition: ModelNotificationConditionInput
  ) {
    updateNotification(input: $input, condition: $condition) {
      id
      assignmentID
      studentID
      levelID
      files
      state
      createdAt
      updatedAt
    }
  }
`;
export const deleteNotification = /* GraphQL */ `
  mutation DeleteNotification(
    $input: DeleteNotificationInput!
    $condition: ModelNotificationConditionInput
  ) {
    deleteNotification(input: $input, condition: $condition) {
      id
      assignmentID
      studentID
      levelID
      files
      state
      createdAt
      updatedAt
    }
  }
`;
export const createExamPool = /* GraphQL */ `
  mutation CreateExamPool(
    $input: CreateExamPoolInput!
    $condition: ModelExamPoolConditionInput
  ) {
    createExamPool(input: $input, condition: $condition) {
      id
      assignmentID
      studentID
      levelID
      examID
      state
      startTime
      createdAt
      updatedAt
    }
  }
`;
export const updateExamPool = /* GraphQL */ `
  mutation UpdateExamPool(
    $input: UpdateExamPoolInput!
    $condition: ModelExamPoolConditionInput
  ) {
    updateExamPool(input: $input, condition: $condition) {
      id
      assignmentID
      studentID
      levelID
      examID
      state
      startTime
      createdAt
      updatedAt
    }
  }
`;
export const deleteExamPool = /* GraphQL */ `
  mutation DeleteExamPool(
    $input: DeleteExamPoolInput!
    $condition: ModelExamPoolConditionInput
  ) {
    deleteExamPool(input: $input, condition: $condition) {
      id
      assignmentID
      studentID
      levelID
      examID
      state
      startTime
      createdAt
      updatedAt
    }
  }
`;
export const createComment = /* GraphQL */ `
  mutation CreateComment(
    $input: CreateCommentInput!
    $condition: ModelCommentConditionInput
  ) {
    createComment(input: $input, condition: $condition) {
      id
      description
      levelID
      userID
      isOwner
      isFile
      createdAt
      updatedAt
    }
  }
`;
export const updateComment = /* GraphQL */ `
  mutation UpdateComment(
    $input: UpdateCommentInput!
    $condition: ModelCommentConditionInput
  ) {
    updateComment(input: $input, condition: $condition) {
      id
      description
      levelID
      userID
      isOwner
      isFile
      createdAt
      updatedAt
    }
  }
`;
export const deleteComment = /* GraphQL */ `
  mutation DeleteComment(
    $input: DeleteCommentInput!
    $condition: ModelCommentConditionInput
  ) {
    deleteComment(input: $input, condition: $condition) {
      id
      description
      levelID
      userID
      isOwner
      isFile
      createdAt
      updatedAt
    }
  }
`;
export const createNewComment = /* GraphQL */ `
  mutation CreateNewComment(
    $input: CreateNewCommentInput!
    $condition: ModelNewCommentConditionInput
  ) {
    createNewComment(input: $input, condition: $condition) {
      id
      commentID
      levelID
      userID
      comment {
        id
        description
        levelID
        userID
        isOwner
        isFile
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateNewComment = /* GraphQL */ `
  mutation UpdateNewComment(
    $input: UpdateNewCommentInput!
    $condition: ModelNewCommentConditionInput
  ) {
    updateNewComment(input: $input, condition: $condition) {
      id
      commentID
      levelID
      userID
      comment {
        id
        description
        levelID
        userID
        isOwner
        isFile
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteNewComment = /* GraphQL */ `
  mutation DeleteNewComment(
    $input: DeleteNewCommentInput!
    $condition: ModelNewCommentConditionInput
  ) {
    deleteNewComment(input: $input, condition: $condition) {
      id
      commentID
      levelID
      userID
      comment {
        id
        description
        levelID
        userID
        isOwner
        isFile
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createNewsLetter = /* GraphQL */ `
  mutation CreateNewsLetter(
    $input: CreateNewsLetterInput!
    $condition: ModelNewsLetterConditionInput
  ) {
    createNewsLetter(input: $input, condition: $condition) {
      id
      name
      email
      createdAt
      updatedAt
    }
  }
`;
export const updateNewsLetter = /* GraphQL */ `
  mutation UpdateNewsLetter(
    $input: UpdateNewsLetterInput!
    $condition: ModelNewsLetterConditionInput
  ) {
    updateNewsLetter(input: $input, condition: $condition) {
      id
      name
      email
      createdAt
      updatedAt
    }
  }
`;
export const deleteNewsLetter = /* GraphQL */ `
  mutation DeleteNewsLetter(
    $input: DeleteNewsLetterInput!
    $condition: ModelNewsLetterConditionInput
  ) {
    deleteNewsLetter(input: $input, condition: $condition) {
      id
      name
      email
      createdAt
      updatedAt
    }
  }
`;
export const createProgram = /* GraphQL */ `
  mutation CreateProgram(
    $input: CreateProgramInput!
    $condition: ModelProgramConditionInput
  ) {
    createProgram(input: $input, condition: $condition) {
      id
      levelID
      code
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const updateProgram = /* GraphQL */ `
  mutation UpdateProgram(
    $input: UpdateProgramInput!
    $condition: ModelProgramConditionInput
  ) {
    updateProgram(input: $input, condition: $condition) {
      id
      levelID
      code
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const deleteProgram = /* GraphQL */ `
  mutation DeleteProgram(
    $input: DeleteProgramInput!
    $condition: ModelProgramConditionInput
  ) {
    deleteProgram(input: $input, condition: $condition) {
      id
      levelID
      code
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const createExam = /* GraphQL */ `
  mutation CreateExam(
    $input: CreateExamInput!
    $condition: ModelExamConditionInput
  ) {
    createExam(input: $input, condition: $condition) {
      id
      levelID
      code
      name
      description
      time
      createdAt
      updatedAt
    }
  }
`;
export const updateExam = /* GraphQL */ `
  mutation UpdateExam(
    $input: UpdateExamInput!
    $condition: ModelExamConditionInput
  ) {
    updateExam(input: $input, condition: $condition) {
      id
      levelID
      code
      name
      description
      time
      createdAt
      updatedAt
    }
  }
`;
export const deleteExam = /* GraphQL */ `
  mutation DeleteExam(
    $input: DeleteExamInput!
    $condition: ModelExamConditionInput
  ) {
    deleteExam(input: $input, condition: $condition) {
      id
      levelID
      code
      name
      description
      time
      createdAt
      updatedAt
    }
  }
`;
export const createIconBox = /* GraphQL */ `
  mutation CreateIconBox(
    $input: CreateIconBoxInput!
    $condition: ModelIconBoxConditionInput
  ) {
    createIconBox(input: $input, condition: $condition) {
      id
      firstTitle
      firstDescription
      secondTitle
      secondDescription
      thirdTitle
      thirdDescription
      createdAt
      updatedAt
    }
  }
`;
export const updateIconBox = /* GraphQL */ `
  mutation UpdateIconBox(
    $input: UpdateIconBoxInput!
    $condition: ModelIconBoxConditionInput
  ) {
    updateIconBox(input: $input, condition: $condition) {
      id
      firstTitle
      firstDescription
      secondTitle
      secondDescription
      thirdTitle
      thirdDescription
      createdAt
      updatedAt
    }
  }
`;
export const deleteIconBox = /* GraphQL */ `
  mutation DeleteIconBox(
    $input: DeleteIconBoxInput!
    $condition: ModelIconBoxConditionInput
  ) {
    deleteIconBox(input: $input, condition: $condition) {
      id
      firstTitle
      firstDescription
      secondTitle
      secondDescription
      thirdTitle
      thirdDescription
      createdAt
      updatedAt
    }
  }
`;
export const createProgramSetting = /* GraphQL */ `
  mutation CreateProgramSetting(
    $input: CreateProgramSettingInput!
    $condition: ModelProgramSettingConditionInput
  ) {
    createProgramSetting(input: $input, condition: $condition) {
      id
      joinLink
      createdAt
      updatedAt
    }
  }
`;
export const updateProgramSetting = /* GraphQL */ `
  mutation UpdateProgramSetting(
    $input: UpdateProgramSettingInput!
    $condition: ModelProgramSettingConditionInput
  ) {
    updateProgramSetting(input: $input, condition: $condition) {
      id
      joinLink
      createdAt
      updatedAt
    }
  }
`;
export const deleteProgramSetting = /* GraphQL */ `
  mutation DeleteProgramSetting(
    $input: DeleteProgramSettingInput!
    $condition: ModelProgramSettingConditionInput
  ) {
    deleteProgramSetting(input: $input, condition: $condition) {
      id
      joinLink
      createdAt
      updatedAt
    }
  }
`;
export const createCommonSetting = /* GraphQL */ `
  mutation CreateCommonSetting(
    $input: CreateCommonSettingInput!
    $condition: ModelCommonSettingConditionInput
  ) {
    createCommonSetting(input: $input, condition: $condition) {
      id
      location
      phone
      email
      facebook
      twitter
      linkedin
      instagram
      youtube
      pinterest
      createdAt
      updatedAt
    }
  }
`;
export const updateCommonSetting = /* GraphQL */ `
  mutation UpdateCommonSetting(
    $input: UpdateCommonSettingInput!
    $condition: ModelCommonSettingConditionInput
  ) {
    updateCommonSetting(input: $input, condition: $condition) {
      id
      location
      phone
      email
      facebook
      twitter
      linkedin
      instagram
      youtube
      pinterest
      createdAt
      updatedAt
    }
  }
`;
export const deleteCommonSetting = /* GraphQL */ `
  mutation DeleteCommonSetting(
    $input: DeleteCommonSettingInput!
    $condition: ModelCommonSettingConditionInput
  ) {
    deleteCommonSetting(input: $input, condition: $condition) {
      id
      location
      phone
      email
      facebook
      twitter
      linkedin
      instagram
      youtube
      pinterest
      createdAt
      updatedAt
    }
  }
`;
export const createSlider = /* GraphQL */ `
  mutation CreateSlider(
    $input: CreateSliderInput!
    $condition: ModelSliderConditionInput
  ) {
    createSlider(input: $input, condition: $condition) {
      id
      image
      createdAt
      updatedAt
    }
  }
`;
export const updateSlider = /* GraphQL */ `
  mutation UpdateSlider(
    $input: UpdateSliderInput!
    $condition: ModelSliderConditionInput
  ) {
    updateSlider(input: $input, condition: $condition) {
      id
      image
      createdAt
      updatedAt
    }
  }
`;
export const deleteSlider = /* GraphQL */ `
  mutation DeleteSlider(
    $input: DeleteSliderInput!
    $condition: ModelSliderConditionInput
  ) {
    deleteSlider(input: $input, condition: $condition) {
      id
      image
      createdAt
      updatedAt
    }
  }
`;
export const createAboutus = /* GraphQL */ `
  mutation CreateAboutus(
    $input: CreateAboutusInput!
    $condition: ModelAboutusConditionInput
  ) {
    createAboutus(input: $input, condition: $condition) {
      id
      title
      description
      companyNumber
      countryNumber
      studentNumber
      challengeNumber
      image
      video
      createdAt
      updatedAt
    }
  }
`;
export const updateAboutus = /* GraphQL */ `
  mutation UpdateAboutus(
    $input: UpdateAboutusInput!
    $condition: ModelAboutusConditionInput
  ) {
    updateAboutus(input: $input, condition: $condition) {
      id
      title
      description
      companyNumber
      countryNumber
      studentNumber
      challengeNumber
      image
      video
      createdAt
      updatedAt
    }
  }
`;
export const deleteAboutus = /* GraphQL */ `
  mutation DeleteAboutus(
    $input: DeleteAboutusInput!
    $condition: ModelAboutusConditionInput
  ) {
    deleteAboutus(input: $input, condition: $condition) {
      id
      title
      description
      companyNumber
      countryNumber
      studentNumber
      challengeNumber
      image
      video
      createdAt
      updatedAt
    }
  }
`;
export const createTestimonial = /* GraphQL */ `
  mutation CreateTestimonial(
    $input: CreateTestimonialInput!
    $condition: ModelTestimonialConditionInput
  ) {
    createTestimonial(input: $input, condition: $condition) {
      id
      title
      description
      avatar
      authorName
      authorTitle
      createdAt
      updatedAt
    }
  }
`;
export const updateTestimonial = /* GraphQL */ `
  mutation UpdateTestimonial(
    $input: UpdateTestimonialInput!
    $condition: ModelTestimonialConditionInput
  ) {
    updateTestimonial(input: $input, condition: $condition) {
      id
      title
      description
      avatar
      authorName
      authorTitle
      createdAt
      updatedAt
    }
  }
`;
export const deleteTestimonial = /* GraphQL */ `
  mutation DeleteTestimonial(
    $input: DeleteTestimonialInput!
    $condition: ModelTestimonialConditionInput
  ) {
    deleteTestimonial(input: $input, condition: $condition) {
      id
      title
      description
      avatar
      authorName
      authorTitle
      createdAt
      updatedAt
    }
  }
`;
export const createQuestion = /* GraphQL */ `
  mutation CreateQuestion(
    $input: CreateQuestionInput!
    $condition: ModelQuestionConditionInput
  ) {
    createQuestion(input: $input, condition: $condition) {
      id
      question
      answer
      createdAt
      updatedAt
    }
  }
`;
export const updateQuestion = /* GraphQL */ `
  mutation UpdateQuestion(
    $input: UpdateQuestionInput!
    $condition: ModelQuestionConditionInput
  ) {
    updateQuestion(input: $input, condition: $condition) {
      id
      question
      answer
      createdAt
      updatedAt
    }
  }
`;
export const deleteQuestion = /* GraphQL */ `
  mutation DeleteQuestion(
    $input: DeleteQuestionInput!
    $condition: ModelQuestionConditionInput
  ) {
    deleteQuestion(input: $input, condition: $condition) {
      id
      question
      answer
      createdAt
      updatedAt
    }
  }
`;
export const createEvent = /* GraphQL */ `
  mutation CreateEvent(
    $input: CreateEventInput!
    $condition: ModelEventConditionInput
  ) {
    createEvent(input: $input, condition: $condition) {
      id
      title
      startTime
      endTime
      location
      description
      month
      day
      createdAt
      updatedAt
    }
  }
`;
export const updateEvent = /* GraphQL */ `
  mutation UpdateEvent(
    $input: UpdateEventInput!
    $condition: ModelEventConditionInput
  ) {
    updateEvent(input: $input, condition: $condition) {
      id
      title
      startTime
      endTime
      location
      description
      month
      day
      createdAt
      updatedAt
    }
  }
`;
export const deleteEvent = /* GraphQL */ `
  mutation DeleteEvent(
    $input: DeleteEventInput!
    $condition: ModelEventConditionInput
  ) {
    deleteEvent(input: $input, condition: $condition) {
      id
      title
      startTime
      endTime
      location
      description
      month
      day
      createdAt
      updatedAt
    }
  }
`;
export const createHelper = /* GraphQL */ `
  mutation CreateHelper(
    $input: CreateHelperInput!
    $condition: ModelHelperConditionInput
  ) {
    createHelper(input: $input, condition: $condition) {
      id
      name
      title
      avatar
      facebook
      twitter
      youtube
      createdAt
      updatedAt
    }
  }
`;
export const updateHelper = /* GraphQL */ `
  mutation UpdateHelper(
    $input: UpdateHelperInput!
    $condition: ModelHelperConditionInput
  ) {
    updateHelper(input: $input, condition: $condition) {
      id
      name
      title
      avatar
      facebook
      twitter
      youtube
      createdAt
      updatedAt
    }
  }
`;
export const deleteHelper = /* GraphQL */ `
  mutation DeleteHelper(
    $input: DeleteHelperInput!
    $condition: ModelHelperConditionInput
  ) {
    deleteHelper(input: $input, condition: $condition) {
      id
      name
      title
      avatar
      facebook
      twitter
      youtube
      createdAt
      updatedAt
    }
  }
`;
export const createBlog = /* GraphQL */ `
  mutation CreateBlog(
    $input: CreateBlogInput!
    $condition: ModelBlogConditionInput
  ) {
    createBlog(input: $input, condition: $condition) {
      id
      title
      smallDescription
      description
      month
      day
      authorName
      commentNumber
      thumbNumber
      image
      createdAt
      updatedAt
    }
  }
`;
export const updateBlog = /* GraphQL */ `
  mutation UpdateBlog(
    $input: UpdateBlogInput!
    $condition: ModelBlogConditionInput
  ) {
    updateBlog(input: $input, condition: $condition) {
      id
      title
      smallDescription
      description
      month
      day
      authorName
      commentNumber
      thumbNumber
      image
      createdAt
      updatedAt
    }
  }
`;
export const deleteBlog = /* GraphQL */ `
  mutation DeleteBlog(
    $input: DeleteBlogInput!
    $condition: ModelBlogConditionInput
  ) {
    deleteBlog(input: $input, condition: $condition) {
      id
      title
      smallDescription
      description
      month
      day
      authorName
      commentNumber
      thumbNumber
      image
      createdAt
      updatedAt
    }
  }
`;
export const createCampus = /* GraphQL */ `
  mutation CreateCampus(
    $input: CreateCampusInput!
    $condition: ModelCampusConditionInput
  ) {
    createCampus(input: $input, condition: $condition) {
      id
      image
      createdAt
      updatedAt
    }
  }
`;
export const updateCampus = /* GraphQL */ `
  mutation UpdateCampus(
    $input: UpdateCampusInput!
    $condition: ModelCampusConditionInput
  ) {
    updateCampus(input: $input, condition: $condition) {
      id
      image
      createdAt
      updatedAt
    }
  }
`;
export const deleteCampus = /* GraphQL */ `
  mutation DeleteCampus(
    $input: DeleteCampusInput!
    $condition: ModelCampusConditionInput
  ) {
    deleteCampus(input: $input, condition: $condition) {
      id
      image
      createdAt
      updatedAt
    }
  }
`;
