/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getUser = /* GraphQL */ `
  query GetUser($id: ID!) {
    getUser(id: $id) {
      id
      name
      email
      avatar
      role
      helpline
      createdAt
      updatedAt
    }
  }
`;
export const listUsers = /* GraphQL */ `
  query ListUsers(
    $filter: ModelUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        email
        avatar
        role
        helpline
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getReplit = /* GraphQL */ `
  query GetReplit($id: ID!) {
    getReplit(id: $id) {
      id
      name
      password
      userID
      createdAt
      updatedAt
    }
  }
`;
export const listReplits = /* GraphQL */ `
  query ListReplits(
    $filter: ModelReplitFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listReplits(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        password
        userID
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getAssignment = /* GraphQL */ `
  query GetAssignment($id: ID!) {
    getAssignment(id: $id) {
      id
      name
      title
      description
      fee
      image
      ownerID
      activate
      review
      studentNumber
      startDate
      Duration
      language
      skillLevel
      subject
      lectures
      enrolled
      createdAt
      updatedAt
    }
  }
`;
export const listAssignments = /* GraphQL */ `
  query ListAssignments(
    $filter: ModelAssignmentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listAssignments(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        title
        description
        fee
        image
        ownerID
        activate
        review
        studentNumber
        startDate
        Duration
        language
        skillLevel
        subject
        lectures
        enrolled
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getSubject = /* GraphQL */ `
  query GetSubject($id: ID!) {
    getSubject(id: $id) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const listSubjects = /* GraphQL */ `
  query ListSubjects(
    $filter: ModelSubjectFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSubjects(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getAssignmentStudent = /* GraphQL */ `
  query GetAssignmentStudent($id: ID!) {
    getAssignmentStudent(id: $id) {
      id
      assignmentID
      studentID
      levelID
      levelState
      state
      createdAt
      updatedAt
    }
  }
`;
export const listAssignmentStudents = /* GraphQL */ `
  query ListAssignmentStudents(
    $filter: ModelAssignmentStudentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listAssignmentStudents(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        assignmentID
        studentID
        levelID
        levelState
        state
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getStudentBlock = /* GraphQL */ `
  query GetStudentBlock($id: ID!) {
    getStudentBlock(id: $id) {
      id
      studentID
      assignmentID
      createdAt
      updatedAt
    }
  }
`;
export const listStudentBlocks = /* GraphQL */ `
  query ListStudentBlocks(
    $filter: ModelStudentBlockFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listStudentBlocks(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        studentID
        assignmentID
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getPart = /* GraphQL */ `
  query GetPart($id: ID!) {
    getPart(id: $id) {
      id
      name
      title
      description
      image
      video
      assignmentID
      levelLength
      createdAt
      updatedAt
    }
  }
`;
export const listParts = /* GraphQL */ `
  query ListParts(
    $filter: ModelPartFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listParts(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        title
        description
        image
        video
        assignmentID
        levelLength
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getPrice = /* GraphQL */ `
  query GetPrice($id: ID!) {
    getPrice(id: $id) {
      id
      price
      type
      assignmentID
      sourceID
      createdAt
      updatedAt
    }
  }
`;
export const listPrices = /* GraphQL */ `
  query ListPrices(
    $filter: ModelPriceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listPrices(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        price
        type
        assignmentID
        sourceID
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getCoupon = /* GraphQL */ `
  query GetCoupon($id: ID!) {
    getCoupon(id: $id) {
      id
      priceID
      code
      isFree
      discountPercentage
      createdAt
      updatedAt
    }
  }
`;
export const listCoupons = /* GraphQL */ `
  query ListCoupons(
    $filter: ModelcouponFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCoupons(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        priceID
        code
        isFree
        discountPercentage
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getPartGroup = /* GraphQL */ `
  query GetPartGroup($id: ID!) {
    getPartGroup(id: $id) {
      id
      partID
      sourceID
      createdAt
      updatedAt
    }
  }
`;
export const listPartGroups = /* GraphQL */ `
  query ListPartGroups(
    $filter: ModelpartGroupFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listPartGroups(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        partID
        sourceID
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getStudentPayment = /* GraphQL */ `
  query GetStudentPayment($id: ID!) {
    getStudentPayment(id: $id) {
      id
      priceID
      studentID
      createdAt
      updatedAt
    }
  }
`;
export const listStudentPayments = /* GraphQL */ `
  query ListStudentPayments(
    $filter: ModelstudentPaymentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listStudentPayments(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        priceID
        studentID
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLevel = /* GraphQL */ `
  query GetLevel($id: ID!) {
    getLevel(id: $id) {
      id
      name
      title
      description
      image
      video
      files
      resDescription
      resImage
      resVideo
      partID
      resFiles
      programLength
      examLength
      createdAt
      updatedAt
    }
  }
`;
export const listLevels = /* GraphQL */ `
  query ListLevels(
    $filter: ModelLevelFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLevels(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        title
        description
        image
        video
        files
        resDescription
        resImage
        resVideo
        partID
        resFiles
        programLength
        examLength
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getNotification = /* GraphQL */ `
  query GetNotification($id: ID!) {
    getNotification(id: $id) {
      id
      assignmentID
      studentID
      levelID
      files
      state
      createdAt
      updatedAt
    }
  }
`;
export const listNotifications = /* GraphQL */ `
  query ListNotifications(
    $filter: ModelNotificationFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listNotifications(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        assignmentID
        studentID
        levelID
        files
        state
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getExamPool = /* GraphQL */ `
  query GetExamPool($id: ID!) {
    getExamPool(id: $id) {
      id
      assignmentID
      studentID
      levelID
      examID
      state
      startTime
      createdAt
      updatedAt
    }
  }
`;
export const listExamPools = /* GraphQL */ `
  query ListExamPools(
    $filter: ModelExamPoolFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listExamPools(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        assignmentID
        studentID
        levelID
        examID
        state
        startTime
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getComment = /* GraphQL */ `
  query GetComment($id: ID!) {
    getComment(id: $id) {
      id
      description
      levelID
      userID
      isOwner
      isFile
      createdAt
      updatedAt
    }
  }
`;
export const listComments = /* GraphQL */ `
  query ListComments(
    $filter: ModelCommentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listComments(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        description
        levelID
        userID
        isOwner
        isFile
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getNewComment = /* GraphQL */ `
  query GetNewComment($id: ID!) {
    getNewComment(id: $id) {
      id
      commentID
      levelID
      userID
      comment {
        id
        description
        levelID
        userID
        isOwner
        isFile
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listNewComments = /* GraphQL */ `
  query ListNewComments(
    $filter: ModelNewCommentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listNewComments(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        commentID
        levelID
        userID
        comment {
          id
          description
          levelID
          userID
          isOwner
          isFile
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getNewsLetter = /* GraphQL */ `
  query GetNewsLetter($id: ID!) {
    getNewsLetter(id: $id) {
      id
      name
      email
      createdAt
      updatedAt
    }
  }
`;
export const listNewsLetters = /* GraphQL */ `
  query ListNewsLetters(
    $filter: ModelNewsLetterFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listNewsLetters(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        email
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getProgram = /* GraphQL */ `
  query GetProgram($id: ID!) {
    getProgram(id: $id) {
      id
      levelID
      code
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const listPrograms = /* GraphQL */ `
  query ListPrograms(
    $filter: ModelProgramFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listPrograms(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        levelID
        code
        name
        description
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getExam = /* GraphQL */ `
  query GetExam($id: ID!) {
    getExam(id: $id) {
      id
      levelID
      code
      name
      description
      time
      createdAt
      updatedAt
    }
  }
`;
export const listExams = /* GraphQL */ `
  query ListExams(
    $filter: ModelExamFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listExams(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        levelID
        code
        name
        description
        time
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getIconBox = /* GraphQL */ `
  query GetIconBox($id: ID!) {
    getIconBox(id: $id) {
      id
      firstTitle
      firstDescription
      secondTitle
      secondDescription
      thirdTitle
      thirdDescription
      createdAt
      updatedAt
    }
  }
`;
export const listIconBoxs = /* GraphQL */ `
  query ListIconBoxs(
    $filter: ModelIconBoxFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listIconBoxs(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        firstTitle
        firstDescription
        secondTitle
        secondDescription
        thirdTitle
        thirdDescription
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getProgramSetting = /* GraphQL */ `
  query GetProgramSetting($id: ID!) {
    getProgramSetting(id: $id) {
      id
      joinLink
      createdAt
      updatedAt
    }
  }
`;
export const listProgramSettings = /* GraphQL */ `
  query ListProgramSettings(
    $filter: ModelProgramSettingFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listProgramSettings(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        joinLink
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getCommonSetting = /* GraphQL */ `
  query GetCommonSetting($id: ID!) {
    getCommonSetting(id: $id) {
      id
      location
      phone
      email
      facebook
      twitter
      linkedin
      instagram
      youtube
      pinterest
      createdAt
      updatedAt
    }
  }
`;
export const listCommonSettings = /* GraphQL */ `
  query ListCommonSettings(
    $filter: ModelCommonSettingFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCommonSettings(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        location
        phone
        email
        facebook
        twitter
        linkedin
        instagram
        youtube
        pinterest
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getSlider = /* GraphQL */ `
  query GetSlider($id: ID!) {
    getSlider(id: $id) {
      id
      image
      createdAt
      updatedAt
    }
  }
`;
export const listSliders = /* GraphQL */ `
  query ListSliders(
    $filter: ModelSliderFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSliders(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        image
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getAboutus = /* GraphQL */ `
  query GetAboutus($id: ID!) {
    getAboutus(id: $id) {
      id
      title
      description
      companyNumber
      countryNumber
      studentNumber
      challengeNumber
      image
      video
      createdAt
      updatedAt
    }
  }
`;
export const listAboutuss = /* GraphQL */ `
  query ListAboutuss(
    $filter: ModelAboutusFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listAboutuss(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        title
        description
        companyNumber
        countryNumber
        studentNumber
        challengeNumber
        image
        video
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getTestimonial = /* GraphQL */ `
  query GetTestimonial($id: ID!) {
    getTestimonial(id: $id) {
      id
      title
      description
      avatar
      authorName
      authorTitle
      createdAt
      updatedAt
    }
  }
`;
export const listTestimonials = /* GraphQL */ `
  query ListTestimonials(
    $filter: ModelTestimonialFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTestimonials(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        title
        description
        avatar
        authorName
        authorTitle
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getQuestion = /* GraphQL */ `
  query GetQuestion($id: ID!) {
    getQuestion(id: $id) {
      id
      question
      answer
      createdAt
      updatedAt
    }
  }
`;
export const listQuestions = /* GraphQL */ `
  query ListQuestions(
    $filter: ModelQuestionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listQuestions(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        question
        answer
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getEvent = /* GraphQL */ `
  query GetEvent($id: ID!) {
    getEvent(id: $id) {
      id
      title
      startTime
      endTime
      location
      description
      month
      day
      createdAt
      updatedAt
    }
  }
`;
export const listEvents = /* GraphQL */ `
  query ListEvents(
    $filter: ModelEventFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listEvents(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        title
        startTime
        endTime
        location
        description
        month
        day
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getHelper = /* GraphQL */ `
  query GetHelper($id: ID!) {
    getHelper(id: $id) {
      id
      name
      title
      avatar
      facebook
      twitter
      youtube
      createdAt
      updatedAt
    }
  }
`;
export const listHelpers = /* GraphQL */ `
  query ListHelpers(
    $filter: ModelHelperFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listHelpers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        title
        avatar
        facebook
        twitter
        youtube
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getBlog = /* GraphQL */ `
  query GetBlog($id: ID!) {
    getBlog(id: $id) {
      id
      title
      smallDescription
      description
      month
      day
      authorName
      commentNumber
      thumbNumber
      image
      createdAt
      updatedAt
    }
  }
`;
export const listBlogs = /* GraphQL */ `
  query ListBlogs(
    $filter: ModelBlogFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listBlogs(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        title
        smallDescription
        description
        month
        day
        authorName
        commentNumber
        thumbNumber
        image
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getCampus = /* GraphQL */ `
  query GetCampus($id: ID!) {
    getCampus(id: $id) {
      id
      image
      createdAt
      updatedAt
    }
  }
`;
export const listCampuss = /* GraphQL */ `
  query ListCampuss(
    $filter: ModelCampusFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCampuss(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        image
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const searchUsers = /* GraphQL */ `
  query SearchUsers(
    $filter: SearchableUserFilterInput
    $sort: SearchableUserSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchUsers(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        name
        email
        avatar
        role
        helpline
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchReplits = /* GraphQL */ `
  query SearchReplits(
    $filter: SearchableReplitFilterInput
    $sort: SearchableReplitSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchReplits(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        name
        password
        userID
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchAssignments = /* GraphQL */ `
  query SearchAssignments(
    $filter: SearchableAssignmentFilterInput
    $sort: SearchableAssignmentSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchAssignments(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        name
        title
        description
        fee
        image
        ownerID
        activate
        review
        studentNumber
        startDate
        Duration
        language
        skillLevel
        subject
        lectures
        enrolled
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchSubjects = /* GraphQL */ `
  query SearchSubjects(
    $filter: SearchableSubjectFilterInput
    $sort: SearchableSubjectSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchSubjects(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        name
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchAssignmentStudents = /* GraphQL */ `
  query SearchAssignmentStudents(
    $filter: SearchableAssignmentStudentFilterInput
    $sort: SearchableAssignmentStudentSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchAssignmentStudents(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        assignmentID
        studentID
        levelID
        levelState
        state
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchStudentBlocks = /* GraphQL */ `
  query SearchStudentBlocks(
    $filter: SearchableStudentBlockFilterInput
    $sort: SearchableStudentBlockSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchStudentBlocks(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        studentID
        assignmentID
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchParts = /* GraphQL */ `
  query SearchParts(
    $filter: SearchablePartFilterInput
    $sort: SearchablePartSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchParts(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        name
        title
        description
        image
        video
        assignmentID
        levelLength
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchPrices = /* GraphQL */ `
  query SearchPrices(
    $filter: SearchablePriceFilterInput
    $sort: SearchablePriceSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchPrices(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        price
        type
        assignmentID
        sourceID
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchCoupons = /* GraphQL */ `
  query SearchCoupons(
    $filter: SearchablecouponFilterInput
    $sort: SearchablecouponSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchCoupons(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        priceID
        code
        isFree
        discountPercentage
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchPartGroups = /* GraphQL */ `
  query SearchPartGroups(
    $filter: SearchablepartGroupFilterInput
    $sort: SearchablepartGroupSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchPartGroups(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        partID
        sourceID
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchStudentPayments = /* GraphQL */ `
  query SearchStudentPayments(
    $filter: SearchablestudentPaymentFilterInput
    $sort: SearchablestudentPaymentSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchStudentPayments(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        priceID
        studentID
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchLevels = /* GraphQL */ `
  query SearchLevels(
    $filter: SearchableLevelFilterInput
    $sort: SearchableLevelSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchLevels(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        name
        title
        description
        image
        video
        files
        resDescription
        resImage
        resVideo
        partID
        resFiles
        programLength
        examLength
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchNotifications = /* GraphQL */ `
  query SearchNotifications(
    $filter: SearchableNotificationFilterInput
    $sort: SearchableNotificationSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchNotifications(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        assignmentID
        studentID
        levelID
        files
        state
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchExamPools = /* GraphQL */ `
  query SearchExamPools(
    $filter: SearchableExamPoolFilterInput
    $sort: SearchableExamPoolSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchExamPools(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        assignmentID
        studentID
        levelID
        examID
        state
        startTime
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchComments = /* GraphQL */ `
  query SearchComments(
    $filter: SearchableCommentFilterInput
    $sort: SearchableCommentSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchComments(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        description
        levelID
        userID
        isOwner
        isFile
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchNewComments = /* GraphQL */ `
  query SearchNewComments(
    $filter: SearchableNewCommentFilterInput
    $sort: SearchableNewCommentSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchNewComments(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        commentID
        levelID
        userID
        comment {
          id
          description
          levelID
          userID
          isOwner
          isFile
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchNewsLetters = /* GraphQL */ `
  query SearchNewsLetters(
    $filter: SearchableNewsLetterFilterInput
    $sort: SearchableNewsLetterSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchNewsLetters(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        name
        email
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchPrograms = /* GraphQL */ `
  query SearchPrograms(
    $filter: SearchableProgramFilterInput
    $sort: SearchableProgramSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchPrograms(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        levelID
        code
        name
        description
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
export const searchExams = /* GraphQL */ `
  query SearchExams(
    $filter: SearchableExamFilterInput
    $sort: SearchableExamSortInput
    $limit: Int
    $nextToken: String
    $from: Int
  ) {
    searchExams(
      filter: $filter
      sort: $sort
      limit: $limit
      nextToken: $nextToken
      from: $from
    ) {
      items {
        id
        levelID
        code
        name
        description
        time
        createdAt
        updatedAt
      }
      nextToken
      total
    }
  }
`;
