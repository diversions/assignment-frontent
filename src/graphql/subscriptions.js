/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateUser = /* GraphQL */ `
  subscription OnCreateUser {
    onCreateUser {
      id
      name
      email
      avatar
      role
      helpline
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateUser = /* GraphQL */ `
  subscription OnUpdateUser {
    onUpdateUser {
      id
      name
      email
      avatar
      role
      helpline
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteUser = /* GraphQL */ `
  subscription OnDeleteUser {
    onDeleteUser {
      id
      name
      email
      avatar
      role
      helpline
      createdAt
      updatedAt
    }
  }
`;
export const onCreateReplit = /* GraphQL */ `
  subscription OnCreateReplit {
    onCreateReplit {
      id
      name
      password
      userID
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateReplit = /* GraphQL */ `
  subscription OnUpdateReplit {
    onUpdateReplit {
      id
      name
      password
      userID
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteReplit = /* GraphQL */ `
  subscription OnDeleteReplit {
    onDeleteReplit {
      id
      name
      password
      userID
      createdAt
      updatedAt
    }
  }
`;
export const onCreateAssignment = /* GraphQL */ `
  subscription OnCreateAssignment {
    onCreateAssignment {
      id
      name
      title
      description
      fee
      image
      ownerID
      activate
      review
      studentNumber
      startDate
      Duration
      language
      skillLevel
      subject
      lectures
      enrolled
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateAssignment = /* GraphQL */ `
  subscription OnUpdateAssignment {
    onUpdateAssignment {
      id
      name
      title
      description
      fee
      image
      ownerID
      activate
      review
      studentNumber
      startDate
      Duration
      language
      skillLevel
      subject
      lectures
      enrolled
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteAssignment = /* GraphQL */ `
  subscription OnDeleteAssignment {
    onDeleteAssignment {
      id
      name
      title
      description
      fee
      image
      ownerID
      activate
      review
      studentNumber
      startDate
      Duration
      language
      skillLevel
      subject
      lectures
      enrolled
      createdAt
      updatedAt
    }
  }
`;
export const onCreateSubject = /* GraphQL */ `
  subscription OnCreateSubject {
    onCreateSubject {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSubject = /* GraphQL */ `
  subscription OnUpdateSubject {
    onUpdateSubject {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSubject = /* GraphQL */ `
  subscription OnDeleteSubject {
    onDeleteSubject {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const onCreateAssignmentStudent = /* GraphQL */ `
  subscription OnCreateAssignmentStudent {
    onCreateAssignmentStudent {
      id
      assignmentID
      studentID
      levelID
      levelState
      state
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateAssignmentStudent = /* GraphQL */ `
  subscription OnUpdateAssignmentStudent {
    onUpdateAssignmentStudent {
      id
      assignmentID
      studentID
      levelID
      levelState
      state
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteAssignmentStudent = /* GraphQL */ `
  subscription OnDeleteAssignmentStudent {
    onDeleteAssignmentStudent {
      id
      assignmentID
      studentID
      levelID
      levelState
      state
      createdAt
      updatedAt
    }
  }
`;
export const onCreateStudentBlock = /* GraphQL */ `
  subscription OnCreateStudentBlock {
    onCreateStudentBlock {
      id
      studentID
      assignmentID
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateStudentBlock = /* GraphQL */ `
  subscription OnUpdateStudentBlock {
    onUpdateStudentBlock {
      id
      studentID
      assignmentID
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteStudentBlock = /* GraphQL */ `
  subscription OnDeleteStudentBlock {
    onDeleteStudentBlock {
      id
      studentID
      assignmentID
      createdAt
      updatedAt
    }
  }
`;
export const onCreatePart = /* GraphQL */ `
  subscription OnCreatePart {
    onCreatePart {
      id
      name
      title
      description
      image
      video
      assignmentID
      levelLength
      createdAt
      updatedAt
    }
  }
`;
export const onUpdatePart = /* GraphQL */ `
  subscription OnUpdatePart {
    onUpdatePart {
      id
      name
      title
      description
      image
      video
      assignmentID
      levelLength
      createdAt
      updatedAt
    }
  }
`;
export const onDeletePart = /* GraphQL */ `
  subscription OnDeletePart {
    onDeletePart {
      id
      name
      title
      description
      image
      video
      assignmentID
      levelLength
      createdAt
      updatedAt
    }
  }
`;
export const onCreatePrice = /* GraphQL */ `
  subscription OnCreatePrice {
    onCreatePrice {
      id
      price
      type
      assignmentID
      sourceID
      createdAt
      updatedAt
    }
  }
`;
export const onUpdatePrice = /* GraphQL */ `
  subscription OnUpdatePrice {
    onUpdatePrice {
      id
      price
      type
      assignmentID
      sourceID
      createdAt
      updatedAt
    }
  }
`;
export const onDeletePrice = /* GraphQL */ `
  subscription OnDeletePrice {
    onDeletePrice {
      id
      price
      type
      assignmentID
      sourceID
      createdAt
      updatedAt
    }
  }
`;
export const onCreateCoupon = /* GraphQL */ `
  subscription OnCreateCoupon {
    onCreateCoupon {
      id
      priceID
      code
      isFree
      discountPercentage
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateCoupon = /* GraphQL */ `
  subscription OnUpdateCoupon {
    onUpdateCoupon {
      id
      priceID
      code
      isFree
      discountPercentage
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteCoupon = /* GraphQL */ `
  subscription OnDeleteCoupon {
    onDeleteCoupon {
      id
      priceID
      code
      isFree
      discountPercentage
      createdAt
      updatedAt
    }
  }
`;
export const onCreatePartGroup = /* GraphQL */ `
  subscription OnCreatePartGroup {
    onCreatePartGroup {
      id
      partID
      sourceID
      createdAt
      updatedAt
    }
  }
`;
export const onUpdatePartGroup = /* GraphQL */ `
  subscription OnUpdatePartGroup {
    onUpdatePartGroup {
      id
      partID
      sourceID
      createdAt
      updatedAt
    }
  }
`;
export const onDeletePartGroup = /* GraphQL */ `
  subscription OnDeletePartGroup {
    onDeletePartGroup {
      id
      partID
      sourceID
      createdAt
      updatedAt
    }
  }
`;
export const onCreateStudentPayment = /* GraphQL */ `
  subscription OnCreateStudentPayment {
    onCreateStudentPayment {
      id
      priceID
      studentID
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateStudentPayment = /* GraphQL */ `
  subscription OnUpdateStudentPayment {
    onUpdateStudentPayment {
      id
      priceID
      studentID
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteStudentPayment = /* GraphQL */ `
  subscription OnDeleteStudentPayment {
    onDeleteStudentPayment {
      id
      priceID
      studentID
      createdAt
      updatedAt
    }
  }
`;
export const onCreateLevel = /* GraphQL */ `
  subscription OnCreateLevel {
    onCreateLevel {
      id
      name
      title
      description
      image
      video
      files
      resDescription
      resImage
      resVideo
      partID
      resFiles
      programLength
      examLength
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateLevel = /* GraphQL */ `
  subscription OnUpdateLevel {
    onUpdateLevel {
      id
      name
      title
      description
      image
      video
      files
      resDescription
      resImage
      resVideo
      partID
      resFiles
      programLength
      examLength
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteLevel = /* GraphQL */ `
  subscription OnDeleteLevel {
    onDeleteLevel {
      id
      name
      title
      description
      image
      video
      files
      resDescription
      resImage
      resVideo
      partID
      resFiles
      programLength
      examLength
      createdAt
      updatedAt
    }
  }
`;
export const onCreateNotification = /* GraphQL */ `
  subscription OnCreateNotification {
    onCreateNotification {
      id
      assignmentID
      studentID
      levelID
      files
      state
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateNotification = /* GraphQL */ `
  subscription OnUpdateNotification {
    onUpdateNotification {
      id
      assignmentID
      studentID
      levelID
      files
      state
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteNotification = /* GraphQL */ `
  subscription OnDeleteNotification {
    onDeleteNotification {
      id
      assignmentID
      studentID
      levelID
      files
      state
      createdAt
      updatedAt
    }
  }
`;
export const onCreateExamPool = /* GraphQL */ `
  subscription OnCreateExamPool {
    onCreateExamPool {
      id
      assignmentID
      studentID
      levelID
      examID
      state
      startTime
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateExamPool = /* GraphQL */ `
  subscription OnUpdateExamPool {
    onUpdateExamPool {
      id
      assignmentID
      studentID
      levelID
      examID
      state
      startTime
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteExamPool = /* GraphQL */ `
  subscription OnDeleteExamPool {
    onDeleteExamPool {
      id
      assignmentID
      studentID
      levelID
      examID
      state
      startTime
      createdAt
      updatedAt
    }
  }
`;
export const onCreateComment = /* GraphQL */ `
  subscription OnCreateComment {
    onCreateComment {
      id
      description
      levelID
      userID
      isOwner
      isFile
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateComment = /* GraphQL */ `
  subscription OnUpdateComment {
    onUpdateComment {
      id
      description
      levelID
      userID
      isOwner
      isFile
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteComment = /* GraphQL */ `
  subscription OnDeleteComment {
    onDeleteComment {
      id
      description
      levelID
      userID
      isOwner
      isFile
      createdAt
      updatedAt
    }
  }
`;
export const onCreateNewComment = /* GraphQL */ `
  subscription OnCreateNewComment {
    onCreateNewComment {
      id
      commentID
      levelID
      userID
      comment {
        id
        description
        levelID
        userID
        isOwner
        isFile
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateNewComment = /* GraphQL */ `
  subscription OnUpdateNewComment {
    onUpdateNewComment {
      id
      commentID
      levelID
      userID
      comment {
        id
        description
        levelID
        userID
        isOwner
        isFile
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteNewComment = /* GraphQL */ `
  subscription OnDeleteNewComment {
    onDeleteNewComment {
      id
      commentID
      levelID
      userID
      comment {
        id
        description
        levelID
        userID
        isOwner
        isFile
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateNewsLetter = /* GraphQL */ `
  subscription OnCreateNewsLetter {
    onCreateNewsLetter {
      id
      name
      email
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateNewsLetter = /* GraphQL */ `
  subscription OnUpdateNewsLetter {
    onUpdateNewsLetter {
      id
      name
      email
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteNewsLetter = /* GraphQL */ `
  subscription OnDeleteNewsLetter {
    onDeleteNewsLetter {
      id
      name
      email
      createdAt
      updatedAt
    }
  }
`;
export const onCreateProgram = /* GraphQL */ `
  subscription OnCreateProgram {
    onCreateProgram {
      id
      levelID
      code
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateProgram = /* GraphQL */ `
  subscription OnUpdateProgram {
    onUpdateProgram {
      id
      levelID
      code
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteProgram = /* GraphQL */ `
  subscription OnDeleteProgram {
    onDeleteProgram {
      id
      levelID
      code
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const onCreateExam = /* GraphQL */ `
  subscription OnCreateExam {
    onCreateExam {
      id
      levelID
      code
      name
      description
      time
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateExam = /* GraphQL */ `
  subscription OnUpdateExam {
    onUpdateExam {
      id
      levelID
      code
      name
      description
      time
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteExam = /* GraphQL */ `
  subscription OnDeleteExam {
    onDeleteExam {
      id
      levelID
      code
      name
      description
      time
      createdAt
      updatedAt
    }
  }
`;
export const onCreateIconBox = /* GraphQL */ `
  subscription OnCreateIconBox {
    onCreateIconBox {
      id
      firstTitle
      firstDescription
      secondTitle
      secondDescription
      thirdTitle
      thirdDescription
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateIconBox = /* GraphQL */ `
  subscription OnUpdateIconBox {
    onUpdateIconBox {
      id
      firstTitle
      firstDescription
      secondTitle
      secondDescription
      thirdTitle
      thirdDescription
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteIconBox = /* GraphQL */ `
  subscription OnDeleteIconBox {
    onDeleteIconBox {
      id
      firstTitle
      firstDescription
      secondTitle
      secondDescription
      thirdTitle
      thirdDescription
      createdAt
      updatedAt
    }
  }
`;
export const onCreateProgramSetting = /* GraphQL */ `
  subscription OnCreateProgramSetting {
    onCreateProgramSetting {
      id
      joinLink
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateProgramSetting = /* GraphQL */ `
  subscription OnUpdateProgramSetting {
    onUpdateProgramSetting {
      id
      joinLink
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteProgramSetting = /* GraphQL */ `
  subscription OnDeleteProgramSetting {
    onDeleteProgramSetting {
      id
      joinLink
      createdAt
      updatedAt
    }
  }
`;
export const onCreateCommonSetting = /* GraphQL */ `
  subscription OnCreateCommonSetting {
    onCreateCommonSetting {
      id
      location
      phone
      email
      facebook
      twitter
      linkedin
      instagram
      youtube
      pinterest
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateCommonSetting = /* GraphQL */ `
  subscription OnUpdateCommonSetting {
    onUpdateCommonSetting {
      id
      location
      phone
      email
      facebook
      twitter
      linkedin
      instagram
      youtube
      pinterest
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteCommonSetting = /* GraphQL */ `
  subscription OnDeleteCommonSetting {
    onDeleteCommonSetting {
      id
      location
      phone
      email
      facebook
      twitter
      linkedin
      instagram
      youtube
      pinterest
      createdAt
      updatedAt
    }
  }
`;
export const onCreateSlider = /* GraphQL */ `
  subscription OnCreateSlider {
    onCreateSlider {
      id
      image
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSlider = /* GraphQL */ `
  subscription OnUpdateSlider {
    onUpdateSlider {
      id
      image
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSlider = /* GraphQL */ `
  subscription OnDeleteSlider {
    onDeleteSlider {
      id
      image
      createdAt
      updatedAt
    }
  }
`;
export const onCreateAboutus = /* GraphQL */ `
  subscription OnCreateAboutus {
    onCreateAboutus {
      id
      title
      description
      companyNumber
      countryNumber
      studentNumber
      challengeNumber
      image
      video
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateAboutus = /* GraphQL */ `
  subscription OnUpdateAboutus {
    onUpdateAboutus {
      id
      title
      description
      companyNumber
      countryNumber
      studentNumber
      challengeNumber
      image
      video
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteAboutus = /* GraphQL */ `
  subscription OnDeleteAboutus {
    onDeleteAboutus {
      id
      title
      description
      companyNumber
      countryNumber
      studentNumber
      challengeNumber
      image
      video
      createdAt
      updatedAt
    }
  }
`;
export const onCreateTestimonial = /* GraphQL */ `
  subscription OnCreateTestimonial {
    onCreateTestimonial {
      id
      title
      description
      avatar
      authorName
      authorTitle
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateTestimonial = /* GraphQL */ `
  subscription OnUpdateTestimonial {
    onUpdateTestimonial {
      id
      title
      description
      avatar
      authorName
      authorTitle
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteTestimonial = /* GraphQL */ `
  subscription OnDeleteTestimonial {
    onDeleteTestimonial {
      id
      title
      description
      avatar
      authorName
      authorTitle
      createdAt
      updatedAt
    }
  }
`;
export const onCreateQuestion = /* GraphQL */ `
  subscription OnCreateQuestion {
    onCreateQuestion {
      id
      question
      answer
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateQuestion = /* GraphQL */ `
  subscription OnUpdateQuestion {
    onUpdateQuestion {
      id
      question
      answer
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteQuestion = /* GraphQL */ `
  subscription OnDeleteQuestion {
    onDeleteQuestion {
      id
      question
      answer
      createdAt
      updatedAt
    }
  }
`;
export const onCreateEvent = /* GraphQL */ `
  subscription OnCreateEvent {
    onCreateEvent {
      id
      title
      startTime
      endTime
      location
      description
      month
      day
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateEvent = /* GraphQL */ `
  subscription OnUpdateEvent {
    onUpdateEvent {
      id
      title
      startTime
      endTime
      location
      description
      month
      day
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteEvent = /* GraphQL */ `
  subscription OnDeleteEvent {
    onDeleteEvent {
      id
      title
      startTime
      endTime
      location
      description
      month
      day
      createdAt
      updatedAt
    }
  }
`;
export const onCreateHelper = /* GraphQL */ `
  subscription OnCreateHelper {
    onCreateHelper {
      id
      name
      title
      avatar
      facebook
      twitter
      youtube
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateHelper = /* GraphQL */ `
  subscription OnUpdateHelper {
    onUpdateHelper {
      id
      name
      title
      avatar
      facebook
      twitter
      youtube
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteHelper = /* GraphQL */ `
  subscription OnDeleteHelper {
    onDeleteHelper {
      id
      name
      title
      avatar
      facebook
      twitter
      youtube
      createdAt
      updatedAt
    }
  }
`;
export const onCreateBlog = /* GraphQL */ `
  subscription OnCreateBlog {
    onCreateBlog {
      id
      title
      smallDescription
      description
      month
      day
      authorName
      commentNumber
      thumbNumber
      image
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateBlog = /* GraphQL */ `
  subscription OnUpdateBlog {
    onUpdateBlog {
      id
      title
      smallDescription
      description
      month
      day
      authorName
      commentNumber
      thumbNumber
      image
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteBlog = /* GraphQL */ `
  subscription OnDeleteBlog {
    onDeleteBlog {
      id
      title
      smallDescription
      description
      month
      day
      authorName
      commentNumber
      thumbNumber
      image
      createdAt
      updatedAt
    }
  }
`;
export const onCreateCampus = /* GraphQL */ `
  subscription OnCreateCampus {
    onCreateCampus {
      id
      image
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateCampus = /* GraphQL */ `
  subscription OnUpdateCampus {
    onUpdateCampus {
      id
      image
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteCampus = /* GraphQL */ `
  subscription OnDeleteCampus {
    onDeleteCampus {
      id
      image
      createdAt
      updatedAt
    }
  }
`;
