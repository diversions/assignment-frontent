import React, {useEffect} from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import {AuthRoute, PrivateRoute, PrivateAdminRoute} from './middleware'
import ScrollToTop from '../helper/ScrollToTop'
import { GlobalStyle } from "../components/common/styles/global.js"
import CourseGrid from '../containers/student/courses/CourseGrid'
import CourseDetails from '../containers/student/courses/CourseDetails'
import PaymentConfirm from '../containers/student/courses/payment/index'
import LevelDetail from '../containers/student/courses/LevelDetail'
// import Detail from '../containers/student/detail'
import Assignment from '../containers/admin/assignment/index'
import AddLevel from '../containers/admin/assignment/level/index'
import AddProgram from '../containers/admin/assignment/program/index'
import AddExam from '../containers/admin/assignment/exam/index'
import ProgramDetail from '../containers/admin/assignment/program/detail/index'
import ExamDetail from '../containers/admin/assignment/exam/detail/index'
import AddPart from '../containers/admin/assignment/part/index'
import Student from '../containers/admin/student/index'
import NewComment from '../containers/admin/comment/index'
import AllComment from '../containers/admin/comment/all'
import CommentDetail from '../containers/admin/comment/detail'
import Notification from '../containers/admin/notification/index'
import ExamPool from '../containers/admin/exampool/index'
import NewsLetter from '../containers/admin/newsLetter/index'
import Replit from '../containers/admin/replit/index'
import Payment from '../containers/admin/payment/index'
import Coupon from '../containers/admin/payment/coupon/index'
import HomeSetting from '../containers/admin/homeSetting/index'
import Home from '../containers/home'
import BlogDetail from '../containers/student/blog/BlogDetails'
import PageNotFound from '../pages/404/PageNotFound'
import ComingSoon from '../pages/comingsoon/ComingSoon'
import Login from '../containers/auth/Login'
import Register from '../containers/auth/Register'
import Logout from '../containers/auth/Logout'
import Confirm from '../containers/auth/Confirm'
import {getCookie} from '../service/cookie'
import {getAuth} from '../service/string'
import {useSetting} from '../provider/setting'

export default function Routes() {
  const [setting, dispatch] = useSetting()

  useEffect(() => {
    const userString = getCookie('auth')
    const user = getAuth(userString)
    if (user) {
      dispatch({type: 'SET', settingName: 'auth', settingData: user})
    }
  }, [])
  return (
    <Router>
      <GlobalStyle />
      <ScrollToTop />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/coming-soon">
          <ComingSoon />
        </Route>
        <Route path="/404">
          <PageNotFound />
        </Route>
        <Route path="/blog/detail/:id">
          <BlogDetail />
        </Route>
        <AuthRoute exact path="/login">
          <Login />
        </AuthRoute>
        <AuthRoute exact path="/register">
          <Register />
        </AuthRoute>
        <AuthRoute exact path="/confirm">
          <Confirm />
        </AuthRoute>
        <PrivateRoute exact path="/logout">
          <Logout />
        </PrivateRoute>
        <PrivateRoute exact path="/course">
          <CourseGrid />
        </PrivateRoute>
        <Route exact path="/payment/:id">
          <PaymentConfirm />
        </Route>
        <PrivateRoute exact path="/course/detail/:id">
          <CourseDetails />
        </PrivateRoute>
        <PrivateRoute exact path="/level/detail/:id">
          <LevelDetail />
        </PrivateRoute>
        <PrivateAdminRoute exact path="/assignment">
          <Assignment />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/assignment/:id">
          <AddPart />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/part/:id">
          <AddLevel />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/level/program/:id">
          <AddProgram />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/level/exam/:id">
          <AddExam />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/program/:id">
          <ProgramDetail />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/exam/:id">
          <ExamDetail />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/student">
          <Student />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/comment">
          <NewComment />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/comment/all">
          <AllComment />
        </PrivateAdminRoute>
        <PrivateAdminRoute path="/comment/:levelID/:userID">
          <CommentDetail />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/notification">
          <Notification />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/exampool">
          <ExamPool />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/newsletter">
          <NewsLetter />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/replit">
          <Replit />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/payment">
          <Payment />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/payment/coupon/:id">
          <Coupon />
        </PrivateAdminRoute>
        <PrivateAdminRoute exact path="/home-setting">
          <HomeSetting />
        </PrivateAdminRoute>
      </Switch>
    </Router>
  );
}
