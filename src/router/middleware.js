import React, {useSelector} from 'react'
import {
  BrowserRouter as Router, 
  Route,
  Redirect,
} from 'react-router-dom'

import {getCookie} from '../service/cookie'
import {getAuth} from '../service/string'

export const AuthRoute = ({ children, ...rest }) => {
  const auth = getAuth(getCookie('auth'))

  return (
    <Route
      {...rest}
      render={({ location }) =>
        !auth ? (
          children
        ) : (
          (
            auth?.role === 'owner' ?
            (
              <Redirect
                to={{
                  pathname: '/assignment',
                  state: { from: location },
                }}
              />
            ):
            (
              <Redirect
                to={{
                  pathname: '/',
                  state: { from: location },
                }}
              />
            )
          )
        )
      }
    />
  );
}

export const PrivateRoute = ({ children, ...rest }) => {
  const auth = getAuth(getCookie('auth'))

  return (
    <Route
      {...rest}
      render={({ location }) =>
        auth ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}

export const PrivateAdminRoute = ({ children, ...rest }) => {
  const auth = getAuth(getCookie('auth'))
  
  return (
    <Route
      {...rest}
      render={({ location }) =>
        auth ?
          (auth?.role === 'owner' ?
          (
            children
          ) :
          <Redirect
            to={{
              pathname: '/',
              state: { from: location },
            }}
          />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}
